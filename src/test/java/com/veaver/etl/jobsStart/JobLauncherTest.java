package com.veaver.etl.jobsStart;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
public class JobLauncherTest {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private JobLauncher jobLauncher;

    @Test
    public void _100_startDepartmentInsertJob() throws Exception {
        startJob("departmentTreeCreateJob");
        startJob("departmentTreeInsertJob");
    }

    @Test
    public void _101_startLogoInsertJob() throws Exception {
        startJob("logoInsertJob");
    }

    @Test
    public void _102_startUserInsertJob() throws Exception {
        startJob("treeIndexCreateJob");
        startJob("userCreateJob");
    }

    @Test
    public void _103_startDeviceInsertjob() throws Exception {
        startJob("deviceInsertjob");
    }

    @Test
    public void _104_startVideoCategoryMigrationJob() throws Exception {
        startJob("VideoCategoryMigrationJob");
    }

    @Test
    public void _105_startBookmarkInsertjob() throws Exception {
        startJob("bookmarkInsertjob");
    }

    @Test
    public void _106_startMultimediaContentMigrationJob() throws Exception {
        startJob("MultimediaContentMigrationJob");
    }

    @Test
    public void _107_startMultimediaTimelineMigrationJob() throws Exception {
        startJob("MultimediaTimelineMigrationJob");
    }

    @Test
    public void _108_startSectionInsertJob() throws Exception {
        startJob("SectionInsertJob");
    }

    @Test
    public void _109_startStackInsertJob() throws Exception {
        startJob("StackInsertJob");
    }

    @Test
    public void _110_startCardInsertJob() throws Exception {
        startJob("CardInsertJob");
    }

    @Test
    public void _111_startEventInsertJob() throws Exception {
        startJob("EventInsertJob");
    }

    @Test
    public void _112_startCompanyContentContractMigration() throws Exception {
        startJob("CompanyContentContractInsertJob");
    }

    @Test
    public void _113_startplaylistInsertjob() throws Exception {
        startJob("playlistInsertjob");
    }

    @Test
    public void _114_startInviteUserListInsertjob() throws Exception {
        startJob("inviteUserListInsertjob");
    }

    @Test
    public void _115_startPlaylistVideoInsertjob() throws Exception {
        startJob("playlistVideoInsertjob");
    }

    @Test
    public void _116_startVideoShareInsertJob() throws Exception {
        startJob("VideoShareInsertJob");
    }

    @Test
    public void _117_startVideoAssignInsertJob() throws Exception {
        startJob("videoAssignInsertJob");
    }

    @Test
    public void _118_startUserVideoActivityInsertJob() throws Exception {
        startJob("UserVideoActivityInsertJob");
    }

    @Test
    public void _119_startUserVideoActivityLogInsertJob() throws Exception {
        startJob("UserVideoActivityLogInsertJob");
    }

    @Test
    public void _120_startTranscodingLogInsertJob() throws Exception {
        startJob("TranscodingLogInsertJob");
    }

    @Test
    public void _121_startUserPushHistoryInsertJob() throws Exception {
        startJob("UserPushHistoryInsertJob");
    }

    @Test
    public void _122_startNoticeInsertJob() throws Exception {
        startJob("NoticeInsertJob");
    }
    private void startJob(String jobName) throws Exception {
        jobName = jobName.trim();
        Job job = (Job) applicationContext.getBean(jobName);
        JobParameters jobParameters = new JobParametersBuilder()
                .addDate("launchDate", new Date())
                .toJobParameters();
        JobExecution jobExecution = jobLauncher.run(job, jobParameters);
        Assert.assertEquals(ExitStatus.COMPLETED,jobExecution.getExitStatus());
    }
}
