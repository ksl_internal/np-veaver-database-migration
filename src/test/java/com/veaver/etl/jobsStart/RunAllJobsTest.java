package com.veaver.etl.jobsStart;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
public class RunAllJobsTest {

    @Value("#{'${spring.batch.job.names:}'.split(',')}")
    private List<String> jobList;

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    ApplicationContext applicationContext;

    @Test
    public void runAllJobs() throws Exception{
        for(String jobName : jobList) {
            jobName = jobName.trim();
            Job job = (Job) applicationContext.getBean(jobName);
            JobParameters jobParameters = new JobParametersBuilder()
                    .addDate("launchDate2", new Date())
                    .toJobParameters();
            JobExecution jobExecution = jobLauncher.run(job, jobParameters);
            Assert.assertEquals(ExitStatus.COMPLETED,jobExecution.getExitStatus());
            System.out.println("===========================");
            System.out.println("Completed Job "+ jobName);
            System.out.println("===========================");
        }
    }
}
