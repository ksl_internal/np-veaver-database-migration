package com.veaver.etl.unitTest;

import com.veaver.etl.department.DepartmentDepth;
import com.veaver.etl.department.DepartmentItemWriter;
import org.junit.Test;

import java.util.Arrays;

public class DepartmentWriterTest {

    @Test
    public void treePopulationTest() throws Exception {
        DepartmentItemWriter itemWriter = new DepartmentItemWriter();
        DepartmentDepth departmentDepth1 = new DepartmentDepth("A1","A2","A3","A4","A5");
        DepartmentDepth departmentDepth2 = new DepartmentDepth("B1","B2","B3","B4","B5");
        DepartmentDepth departmentDepth3 = new DepartmentDepth("A1","A2","C3","A4","A5");
        itemWriter.write(Arrays.asList(departmentDepth1, departmentDepth2, departmentDepth3));
    }
}
