RENAME TABLE  V2_DIVISIONAL_HIERARCHY  TO T_DIVISIONAL_HIERARCHY ;
RENAME TABLE  V2_USER_PUSH_HISTORY TO T_USER_PUSH_HISTORY;
RENAME TABLE  V2_TRANSCODING_LOG TO T_TRANSCODING_LOG;
RENAME TABLE  V2_NOTICE TO T_NOTICE;
RENAME TABLE  V2_USER_VIDEO_ACTIVITY TO T_USER_VIDEO_ACTIVITY;
RENAME TABLE  V2_USER_VIDEO_ACTIVITY_LOG TO T_USER_VIDEO_ACTIVITY_LOG;
RENAME TABLE  V2_USER_VIDEO_ASSIGN TO T_USER_VIDEO_ASSIGN;
RENAME TABLE  V2_USER_VIDEO_SHARE TO T_USER_VIDEO_SHARE;
RENAME TABLE  V2_INVITE_USER_LIST TO T_INVITE_USER_LIST;
RENAME TABLE  V2_PLAYLIST_VIDEO TO T_PLAYLIST_VIDEO;
RENAME TABLE  V2_PLAYLIST TO T_PLAYLIST;
RENAME TABLE  V2_CARD TO T_CARD;
RENAME TABLE  V2_COMPANY_CONTENT_CONTRACT TO T_COMPANY_CONTENT_CONTRACT;
RENAME TABLE  V2_EVENT TO T_EVENT;
RENAME TABLE  V2_STACK TO T_STACK;
RENAME TABLE  V2_SECTION TO T_SECTION;
RENAME TABLE  V2_MULTIMEDIA_TIMELINE TO T_MULTIMEDIA_TIMELINE;
RENAME TABLE  V2_MULTIMEDIA_CONTENT TO T_MULTIMEDIA_CONTENT;
RENAME TABLE  V2_MULTIMEDIA_CATEGORY TO T_MULTIMEDIA_CATEGORY;
RENAME TABLE  V2_DEVICE TO T_DEVICE;
RENAME TABLE  V2_USER_BOOKMARK TO T_USER_BOOKMARK;
RENAME TABLE  V2_OTP TO T_OTP;
RENAME TABLE  V2_USER TO T_USER;
RENAME TABLE  V2_DEPARTMENT TO T_DEPARTMENT;
RENAME TABLE  V2_COMPANY_CONTRACT TO T_COMPANY_CONTRACT;
RENAME TABLE  V2_COMPANY_ROLE  TO T_COMPANY_ROLE ;
RENAME TABLE  V2_COMPANY_IMAGE  TO T_COMPANY_IMAGE ;
RENAME TABLE  V2_POSITION TO T_POSITION;
RENAME TABLE  V2_COMPANY  TO T_COMPANY ;