ALTER TABLE USER DROP COLUMN ETL_STATUS;
ALTER TABLE DEVICE DROP COLUMN ETL_STATUS;
ALTER TABLE VIEW_VIDEO_CATEGORY DROP COLUMN ETL_STATUS;
ALTER TABLE MST_USER_BOOKMARK DROP COLUMN ETL_STATUS;
ALTER TABLE VIDEO DROP COLUMN ETL_STATUS;
ALTER TABLE TIMELINE DROP COLUMN ETL_STATUS;
ALTER TABLE TIMELINE DROP COLUMN ETL_STATUS_2;
ALTER TABLE SECTION DROP COLUMN ETL_STATUS;
ALTER TABLE STACK DROP COLUMN ETL_STATUS;
ALTER TABLE CARD DROP COLUMN ETL_STATUS;
ALTER TABLE EVENT DROP COLUMN ETL_STATUS;
ALTER TABLE FOLDER DROP COLUMN ETL_STATUS;
ALTER TABLE INVITE_GROUP_FOLDER DROP COLUMN ETL_STATUS;
ALTER TABLE PIN DROP COLUMN ETL_STATUS;
ALTER TABLE SHARED DROP COLUMN ETL_STATUS;
ALTER TABLE ASSIGNED DROP COLUMN ETL_STATUS;
ALTER TABLE USER_TIMELINE DROP COLUMN ETL_STATUS;
ALTER TABLE USER_ACTION_LOG DROP COLUMN ETL_STATUS;

ALTER TABLE TRANSCODING_LOG DROP COLUMN ETL_STATUS;
ALTER TABLE USER_PUSH_HISTORY DROP COLUMN ETL_STATUS;
ALTER TABLE NOTICE DROP COLUMN ETL_STATUS;

ALTER TABLE USER ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE DEVICE ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE VIEW_VIDEO_CATEGORY ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE MST_USER_BOOKMARK ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE VIDEO ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE TIMELINE ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE TIMELINE ADD ETL_STATUS_2 INT DEFAULT 0;
ALTER TABLE SECTION ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE STACK ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE CARD ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE EVENT ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE FOLDER ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE INVITE_GROUP_FOLDER ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE PIN ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE SHARED ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE ASSIGNED ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE USER_TIMELINE ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE USER_ACTION_LOG ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE TRANSCODING_LOG ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE USER_PUSH_HISTORY ADD ETL_STATUS INT DEFAULT 0;
ALTER TABLE NOTICE ADD ETL_STATUS INT DEFAULT 0;


UPDATE USER SET ETL_STATUS = 0;
UPDATE DEVICE SET ETL_STATUS = 0;
UPDATE VIEW_VIDEO_CATEGORY SET ETL_STATUS = 0;
UPDATE MST_USER_BOOKMARK SET ETL_STATUS = 0;
UPDATE VIDEO SET ETL_STATUS = 0;
UPDATE TIMELINE SET ETL_STATUS = 0;
UPDATE TIMELINE SET ETL_STATUS_2 = 0;
UPDATE SECTION SET ETL_STATUS = 0;
UPDATE STACK SET ETL_STATUS = 0;
UPDATE CARD SET ETL_STATUS = 0;
UPDATE EVENT SET ETL_STATUS = 0;
UPDATE FOLDER SET ETL_STATUS = 0;
UPDATE INVITE_GROUP_FOLDER SET ETL_STATUS = 0;
UPDATE PIN SET ETL_STATUS = 0;
UPDATE SHARED SET ETL_STATUS = 0;
UPDATE ASSIGNED SET ETL_STATUS = 0;
UPDATE USER_TIMELINE SET ETL_STATUS = 0;
UPDATE USER_ACTION_LOG SET ETL_STATUS = 0;
UPDATE TRANSCODING_LOG SET ETL_STATUS = 0;
UPDATE USER_PUSH_HISTORY SET ETL_STATUS = 0;
UPDATE NOTICE SET ETL_STATUS = 0;