package com.veaver.etl.department;

import com.veaver.etl.configuration.ApplicationProperties;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DepartmentTreeInsertTasklet implements Tasklet {

    private SimpleJdbcInsert simpleJdbcInsert;
    private ApplicationProperties applicationProperties;

    private final String TABLE_NAME = "T_DIVISIONAL_HIERARCHY";
    private final String KEY_COLUMN = "ID";

    private final String NAME = "NAME";
    private final String LEVEL = "LEVEL";
    private final String COM_ID = "COM_ID";
    private final String PARENT_DEPT_ID = "PARENT_ID";
    private final String VERSION = "VERSION";
    private final String TYPE = "TYPE";
    private final String REG_DATE = "REG_DATE";

    public DepartmentTreeInsertTasklet(@Qualifier(value = "tobeJdbcTemplate") JdbcTemplate jdbcTemplate, ApplicationProperties applicationProperties) {
        this.simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate).withTableName(TABLE_NAME).usingGeneratedKeyColumns(KEY_COLUMN);
        this.applicationProperties = applicationProperties;
    }

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

        for(MyTreeNode treeNode : DepartmentItemWriter.root.getChildren()){
            Map<String, Object> parameterMap = new HashMap<>();
            parameterMap.put(NAME, treeNode.getData());
            parameterMap.put(LEVEL, 0);
            parameterMap.put(COM_ID, applicationProperties.getCompanyId());
            parameterMap.put(VERSION, 0);
            parameterMap.put(TYPE, "DEPARTMENT");
            parameterMap.put(REG_DATE, new Date());
            int key = simpleJdbcInsert.executeAndReturnKey(parameterMap).intValue();
            insertChildDepartments(treeNode, 1, key);
        }
        return RepeatStatus.FINISHED;
    }

    private void insertChildDepartments(MyTreeNode<String> parent, int level, int parentKey) {

        if(parent == null || parent.getChildren() == null || parent.getChildren().isEmpty())
            return;

        for(MyTreeNode treeNode : parent.getChildren()){
            Map<String, Object> parameterMap = new HashMap<>();
            parameterMap.put(NAME, treeNode.getData());
            parameterMap.put(LEVEL, level);
            parameterMap.put(COM_ID, applicationProperties.getCompanyId());
            parameterMap.put(PARENT_DEPT_ID, parentKey);
            parameterMap.put(VERSION, 0);
            parameterMap.put(TYPE, "DEPARTMENT");
            parameterMap.put(REG_DATE, new Date());
            int key = simpleJdbcInsert.executeAndReturnKey(parameterMap).intValue();
            insertChildDepartments(treeNode, level+1, key);
        }
    }
}
