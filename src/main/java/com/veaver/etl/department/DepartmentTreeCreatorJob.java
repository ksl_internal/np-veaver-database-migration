package com.veaver.etl.department;

import com.veaver.etl.configuration.BaseJobConfiguration;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.PassThroughItemProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DepartmentTreeCreatorJob extends BaseJobConfiguration {


    private JdbcCursorItemReader<DepartmentDepth> databaseReader() {
        return departmentReader(asisDataSource, "SELECT 1_DEPTH, 2_DEPTH, 3_DEPTH, 4_DEPTH, 5_DEPTH FROM USER_PROFILE;");
    }

    private JdbcCursorItemReader<DepartmentDepth> departmentReader(DataSource ds, String sql) {
        JdbcCursorItemReader<DepartmentDepth> reader = new JdbcCursorItemReader<>();
        reader.setSql(sql);
        reader.setDataSource(ds);
        reader.setRowMapper((rs, rowNum) -> {
            DepartmentDepth departmentDepth = new DepartmentDepth();
            departmentDepth.set_1_DEPTH(rs.getString(1));
            departmentDepth.set_2_DEPTH(rs.getString(2));
            departmentDepth.set_3_DEPTH(rs.getString(3));
            departmentDepth.set_4_DEPTH(rs.getString(4));
            departmentDepth.set_5_DEPTH(rs.getString(5));
            return departmentDepth;
        });
        return reader;
    }

    @Bean("departmentTreeCreateJob")
    public Job treeCreatorJob() {
        return jobBuilderFactory.get("departmentTreeCreatorJob")
                .incrementer(new RunIdIncrementer())
                .start(step1())
                .build();
    }

    @Bean("departmentTreeInsertJob")
    public Job treeInsertJob() {
        return jobBuilderFactory.get("departmentTreeInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(treeInsertStep1())
                .build();
    }

    private Step step1() {
        return stepBuilderFactory.get("treeCreatorStep1")
                .<DepartmentDepth, DepartmentDepth>chunk(500)
                .reader(databaseReader())
                .processor(new PassThroughItemProcessor<>())
                .writer(new DepartmentItemWriter())
                .build();
    }

    private Step treeInsertStep1() {
        return stepBuilderFactory.get("treeInsertStep1")
                .tasklet(new DepartmentTreeInsertTasklet(jdbcTemplate,applicationProperties))
                .build();
    }
}
