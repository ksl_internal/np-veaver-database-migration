package com.veaver.etl.department;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class DepartmentItemWriter implements ItemWriter<DepartmentDepth> {

    public static MyTreeNode<String> root = new MyTreeNode<>("root");

    @Override
    public void write(List<? extends DepartmentDepth> items) throws Exception {
        for(DepartmentDepth item : items){
            List<String> dataList = getDepthAsList(item);
            addToTree(root, dataList, 0);
        }
        log.debug("{}",root);
    }

    private void addToTree(MyTreeNode<String> start, List<String> dataList, int index){
        if(index >= dataList.size())
            return;
        String data = dataList.get(index);
        MyTreeNode child = start.getChild(data);
        if(child == null){
            start.addChild(data);
            child = start.getChild(data);
        }
        addToTree(child, dataList, index+1);
    }

    private List<String> getDepthAsList(DepartmentDepth departmentDepth){
        List<String> result = new ArrayList<>();
        if(departmentDepth.get_1_DEPTH() == null)
            return result;
        result.add(departmentDepth.get_1_DEPTH());

        if(departmentDepth.get_2_DEPTH() == null)
            return result;
        result.add(departmentDepth.get_2_DEPTH());

        if(departmentDepth.get_3_DEPTH() == null)
            return result;
        result.add(departmentDepth.get_3_DEPTH());

        if(departmentDepth.get_4_DEPTH() == null)
            return result;
        result.add(departmentDepth.get_4_DEPTH());

        if(departmentDepth.get_5_DEPTH() == null)
            return result;
        result.add(departmentDepth.get_5_DEPTH());

        return result;
    }
}
