package com.veaver.etl.department;

import java.util.ArrayList;
import java.util.List;

public class MyTreeNode<T>{
    private T data = null;
    private List<MyTreeNode> children = new ArrayList<>();
    private MyTreeNode parent = null;

    public MyTreeNode(T data) {
        this.data = data;
    }

    public void addChild(MyTreeNode child) {
        child.setParent(this);
        this.children.add(child);
    }

    public void addChild(T data) {
        MyTreeNode<T> newChild = new MyTreeNode<>(data);
        this.addChild(newChild);
    }

    public void addChildren(List<MyTreeNode> children) {
        for(MyTreeNode t : children) {
            t.setParent(this);
        }
        this.children.addAll(children);
    }

    public List<MyTreeNode> getChildren() {
        return children;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    private void setParent(MyTreeNode parent) {
        this.parent = parent;
    }

    public MyTreeNode getParent() {
        return parent;
    }

    public boolean hasChild(T data){
        for(MyTreeNode child : children){
            if(child.getData().equals(data))
                return true;
        }
        return false;
    }

    public MyTreeNode getChild(T data){
        for(MyTreeNode child : children){
            if(child.getData().equals(data))
                return child;
        }
        return null;
    }

    @Override
    public String toString() {
        return String.format("[%s]->[%s]", data, printChild() );
    }

    private Object printChild() {
        StringBuilder result = new StringBuilder();
        for (MyTreeNode treeNode : children){
            result.append(treeNode.toString());
        }
        return result.toString();
    }


}
