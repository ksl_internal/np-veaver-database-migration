package com.veaver.etl.department;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentDepth {
    String _1_DEPTH;
    String _2_DEPTH;
    String _3_DEPTH;
    String _4_DEPTH;
    String _5_DEPTH;
}
