package com.veaver.etl.transcodinglog;

import com.veaver.etl.configuration.BaseJobConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;


@Slf4j
@Configuration
public class TranscodingLogMigrationJob extends BaseJobConfiguration {

    private JdbcCursorItemReader<AsisTranscodingLog> databaseReader() {
        return databaseReader(asisDataSource,
                "SELECT IDX, FILE_URL, STATUS, DURATION, WIDTH, HEIGHT, VIDEO_IDX, REG_DATE, TYPE, META \n" +
                        "FROM TRANSCODING_LOG WHERE ETL_STATUS=0",
                AsisTranscodingLog.class);
    }

    private JdbcBatchItemWriter<TobeTranscodingLog> insertTobeWriter() {
        return databaseWriter(tobeDataSource, "INSERT INTO T_TRANSCODING_LOG\n" +
                "(FILE_URL, STATUS, DURATION, WIDTH, HEIGHT, REG_DATE, TYPE, META, OLD_ID, ETL_COM_ID, " +
                "MULTIMEDIA_CONTENT_ID)\n" +
                "VALUES\n" +
                "(:file_url, :status, :duration, :width,:height,:reg_date, :type, :meta, :old_id, :com_id," +
                "(SELECT ID FROM T_MULTIMEDIA_CONTENT MC WHERE MC.OLD_ID=:video_idx AND MC.ETL_COM_ID=:com_id))");
    }
    private JdbcBatchItemWriter<TobeTranscodingLog> updateAsisWriter() {
        return databaseWriter(asisDataSource, "Update TRANSCODING_LOG SET ETL_STATUS=1 WHERE IDX=:old_id");
    }

    private CompositeItemWriter<TobeTranscodingLog> dataBaseWriter() {
        CompositeItemWriter<TobeTranscodingLog> compositeItemWriter = new CompositeItemWriter<>();
        compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        return compositeItemWriter;
    }

    @Bean(value = "TranscodingLogInsertJob")
    public Job job1() {
        return jobBuilderFactory.get("TranscodingLogInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(step1())
                .build();
    }

    private Step step1() {
        return stepBuilderFactory.get("TranscodingLogInsertStep")
                .<AsisTranscodingLog, TobeTranscodingLog>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<AsisTranscodingLog, TobeTranscodingLog>) item -> {
                    log.debug("{}",item);
                    TobeTranscodingLog content = TobeTranscodingLog.builder()
                            .com_id(applicationProperties.getCompanyId())
                            .meta(item.getMeta())
                            .old_id(item.getIdx())
                            .reg_date(longToDate(item.getReg_date()))
                            .type(item.getType())
                            .file_url(item.getFile_url())
                            .status(item.getStatus())
                            .height(item.getHeight())
                            .width(item.getWidth())
                            .duration(item.getDuration())
                            .video_idx(item.getVideo_idx())
                            .build();

                    log.debug("{}", content);
                    return content;
                })
                .writer(dataBaseWriter())
                .build();
    }
}

