package com.veaver.etl.transcodinglog;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TobeTranscodingLog {
    String file_url;
    String status;
    Long duration;
    Integer width;
    Integer height;
    Long video_idx;
    Date reg_date;
    String type;
    String meta;
    Long old_id;
    String com_id;
}
