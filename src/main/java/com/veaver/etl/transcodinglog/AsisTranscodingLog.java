package com.veaver.etl.transcodinglog;

import lombok.Data;

@Data
public class AsisTranscodingLog {
    Long idx;
    String file_url;
    String status;
    Long duration;
    Integer width;
    Integer height;
    Long video_idx;
    Long reg_date;
    String type;
    String meta;

}
