package com.veaver.etl.userpushhistory;

import lombok.Data;

@Data
public class AsisUserPushHistory {
    Long idx;
    String user_key;
    String from_user_key;
    String message_code;
    String message;
    String data;
    String status;
    String view_flag;
    String confirm_flag;
    Long reg_date;
}
