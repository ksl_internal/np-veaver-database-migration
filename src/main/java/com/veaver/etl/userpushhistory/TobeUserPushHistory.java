package com.veaver.etl.userpushhistory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TobeUserPushHistory {
    String user_key;
    String from_user_key;
    String message_code;
    String message;
    String data;
    String status;
    String view_flag;
    String confirm_flag;
    Date reg_date;
    Long old_id;
    String com_id;
}
