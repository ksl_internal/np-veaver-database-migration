package com.veaver.etl.userpushhistory;

import com.veaver.etl.configuration.BaseJobConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Slf4j
@Configuration
public class UserPushHistoryMigrationJob extends BaseJobConfiguration {
    private JdbcCursorItemReader<AsisUserPushHistory> databaseReader() {
        return databaseReader(asisDataSource,
                "SELECT IDX, USER_KEY, FROM_USER_KEY,MESSAGE_CODE,MESSAGE,DATA,STATUS,VIEW_FLAG, CONFIRM_FLAG, REG_DATE FROM USER_PUSH_HISTORY WHERE ETL_STATUS=0",
                AsisUserPushHistory.class);
    }

    private JdbcBatchItemWriter<TobeUserPushHistory> insertTobeWriter() {
        return databaseWriter(tobeDataSource, "INSERT INTO T_USER_PUSH_HISTORY\n" +
                "( USER_ID, MESSAGE_CODE,MESSAGE,DATA,STATUS, CONFIRM_FLAG, REG_DATE, OLD_ID, ETL_COM_ID)\n" +
                "VALUES\n" +
                "((SELECT ID FROM T_USER U WHERE U.USER_KEY=:user_key AND U.ETL_COM_ID=:com_id),\n" +
                " :message_code,:message,:data,:status,:confirm_flag,:reg_date,:old_id,:com_id)");
    }
    private JdbcBatchItemWriter<TobeUserPushHistory> updateAsisWriter() {
        return databaseWriter(asisDataSource, "Update USER_PUSH_HISTORY SET ETL_STATUS=1 WHERE IDX=:old_id");
    }

    private CompositeItemWriter<TobeUserPushHistory> dataBaseWriter() {
        CompositeItemWriter<TobeUserPushHistory> compositeItemWriter = new CompositeItemWriter<>();
        compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        return compositeItemWriter;
    }

    @Bean(value = "UserPushHistoryInsertJob")
    public Job job1() {
        return jobBuilderFactory.get("UserPushHistoryInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(step1())
                .build();
    }

    private Step step1() {
        return stepBuilderFactory.get("UserPushHistoryInsertStep")
                .<AsisUserPushHistory, TobeUserPushHistory>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<AsisUserPushHistory, TobeUserPushHistory>) item -> {
                    log.debug("{}",item);
                    TobeUserPushHistory content = TobeUserPushHistory.builder()
                            .com_id(applicationProperties.getCompanyId())
                            .old_id(item.getIdx())
                            .reg_date(longToDate(item.getReg_date()))
                            .status(item.getStatus())
                            .user_key(item.getUser_key())
                            .from_user_key(item.getFrom_user_key())
                            .message_code(item.getMessage_code())
                            .message(item.getMessage())
                            .data(item.getData())
                            .view_flag(item.getView_flag())
                            .confirm_flag(item.getConfirm_flag())
                            .build();

                    log.debug("{}", content);
                    return content;
                })
                .writer(dataBaseWriter())
                .build();
    }
}
