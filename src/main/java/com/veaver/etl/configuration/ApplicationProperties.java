package com.veaver.etl.configuration;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class ApplicationProperties {

    @Value("${etl.company.id}")
    private String companyId;

    @Value("${etl.company.role.code}")
    private String companyRoleCode;

    @Value("${etl.tool.status.update.enabled:true}")
    private Boolean statusUpdateEnabled;

}
