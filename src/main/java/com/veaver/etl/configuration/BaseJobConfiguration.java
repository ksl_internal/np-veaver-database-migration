package com.veaver.etl.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.Date;

@Slf4j
@Configuration
public abstract class BaseJobConfiguration {

    @Autowired
    protected JobBuilderFactory jobBuilderFactory;

    @Autowired
    protected StepBuilderFactory stepBuilderFactory;

    @Autowired
    @Qualifier(value = "asisDataSource")
    protected DataSource asisDataSource;

    @Autowired
    @Qualifier(value = "tobeDataSource")
    protected DataSource tobeDataSource;

    @Autowired
    @Qualifier(value = "tobeJdbcTemplate")
    protected JdbcTemplate jdbcTemplate;

    @Autowired
    protected ApplicationProperties applicationProperties;

    protected ObjectMapper objectMapper = new ObjectMapper();


    protected  <T> JdbcBatchItemWriter<T> databaseWriter(DataSource ds, String sql) {
        JdbcBatchItemWriter<T> writer = new JdbcBatchItemWriter<>();
        writer.setSql(sql);
        writer.setDataSource(ds);
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<T>());
        writer.afterPropertiesSet();
        return writer;
    }

    protected <T> JdbcCursorItemReader<T> databaseReader(DataSource ds, String sql, Class<T> tClass) {
        JdbcCursorItemReader<T> reader = new JdbcCursorItemReader<>();
        reader.setSql(sql);
        reader.setDataSource(ds);
        reader.setRowMapper(new BeanPropertyRowMapper<>(tClass));
        return reader;
    }

    protected Date longToDate(Long millis){
        if(millis==null)
            return null;
        return new Date(millis);
    }
}
