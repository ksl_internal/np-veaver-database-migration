package com.veaver.etl.configuration;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class ApplicationConfiguration {

    @Primary
    @Bean(value = "defaultDataSource")
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource datasource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        return dataSource;
    }

    @Bean(value = "asisDataSource")
    @ConfigurationProperties(prefix = "asis.datasource")
    public DataSource asisDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        return dataSource;
    }

    @Bean(value = "tobeDataSource")
    @ConfigurationProperties(prefix = "tobe.datasource")
    public DataSource tobeDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        return dataSource;
    }

    @Bean(value = "tobeJdbcTemplate")
    public JdbcTemplate tobeJdbcTemplate() {
        return new JdbcTemplate(tobeDataSource());
    }

    @Bean(value = "tobeTransactionManager")
    public DataSourceTransactionManager tobeTransactionManager() {
        return new DataSourceTransactionManager(tobeDataSource());
    }
}
