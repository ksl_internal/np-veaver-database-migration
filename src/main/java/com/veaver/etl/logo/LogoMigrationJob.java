package com.veaver.etl.logo;

import com.veaver.etl.configuration.BaseJobConfiguration;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LogoMigrationJob extends BaseJobConfiguration {

    @Autowired
    LogoProcessor logoProcessor;

    private JdbcCursorItemReader<Logo> databaseReader() {
        return databaseReader(asisDataSource, "SELECT * FROM LOGO;", Logo.class);
    }

    private JdbcBatchItemWriter<CompanyImage> dataBaseWriter() {
        return databaseWriter(tobeDataSource, "INSERT INTO T_COMPANY_IMAGE " +
                "( COM_ID ,  IMAGE_TYPE,  IMAGE_NAME ,  IMAGE_URL , SIZE ,  REG_DATE) " +
                "VALUES " +
                "( :com_id, :image_type, :image_name , :image_url , :size, :reg_date)");
    }

    @Bean("logoInsertJob")
    public Job logoInsertJob() {
        return jobBuilderFactory.get("logoInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(logoProcessingStep())
                .build();
    }

    private Step logoProcessingStep() {
        return stepBuilderFactory.get("logoProcessingStep")
                .<Logo, CompanyImage>chunk(500)
                .reader(databaseReader())
                .processor(logoProcessor)
                .writer(dataBaseWriter())
                .build();
    }
}
