package com.veaver.etl.logo;

import lombok.Data;

import java.util.Date;

@Data
public class CompanyImage {
    String com_id;
    String image_type;
    String image_name;
    String image_url;
    Date reg_date;
    Date upd_date;
    Long size;
}
