package com.veaver.etl.logo;

import com.veaver.etl.configuration.ApplicationProperties;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class LogoProcessor implements ItemProcessor<Logo, CompanyImage> {

    @Autowired
    ApplicationProperties applicationProperties;

    @Override
    public CompanyImage process(Logo item) throws Exception {
        CompanyImage companyImage = new CompanyImage();
        companyImage.setCom_id(applicationProperties.getCompanyId());
        companyImage.setImage_name(item.name);
        companyImage.setImage_type(item.os_type);
        companyImage.setImage_url(item.getPath());
        companyImage.setReg_date(new Date(item.getReg_date()));
        companyImage.setSize(item.getSize());
        return companyImage;
    }
}
