package com.veaver.etl.logo;

import lombok.Data;

@Data
public class Logo {
    String os_type;
    String name;
    String path;
    Long size;
    Long reg_date;
}
