package com.veaver.etl.notice;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TobeNotice {
    String title;
    String reg_user_key;
    Date reg_date;
    String upt_user_key;
    Date upt_date;
    Date view_date;
    Date view_end_date;
    String notice_type;
    Long timeline_idx;
    String editor_detail;
    String push_flag;
    Long old_id;
    String com_id;
}
