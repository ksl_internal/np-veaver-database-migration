package com.veaver.etl.notice;

import com.veaver.etl.configuration.BaseJobConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
@Slf4j
@Configuration
public class NoticeMigrationJob extends BaseJobConfiguration {
    private JdbcCursorItemReader<AsisNotice> databaseReader() {
        return databaseReader(asisDataSource,
                "SELECT IDX, " +
                        "TITLE, " +
                        "REG_USER_KEY, " +
                        "REG_DATE, " +
                        "UPT_USER_KEY, " +
                        "UPT_DATE, " +
                        "VIEW_DATE, " +
                        "VIEW_END_DATE, " +
                        "NOTICE_TYPE, " +
                        "TIMELINE_IDX, " +
                        "EDITOR_DETAIL, " +
                        "PUSH_FLAG " +
                        " FROM NOTICE WHERE ETL_STATUS=0",
                AsisNotice.class);
    }

    private JdbcBatchItemWriter<TobeNotice> insertTobeWriter() {
        return databaseWriter(tobeDataSource, "INSERT INTO T_NOTICE\n" +
                "(TITLE, REG_USER_ID, REG_DATE, UPT_USER_ID,UPT_DATE,VIEW_DATE,VIEW_END_DATE, NOTICE_TYPE,\n" +
                " CONTENT_CONTRACT_ID, EDITOR_DETAIL,PUSH_FLAG, OLD_ID, ETL_COM_ID)\n" +
                "VALUES\n" +
                "(:title,\n" +
                "(SELECT ID FROM T_USER U WHERE U.USER_KEY=:reg_user_key AND U.ETL_COM_ID=:com_id),\n" +
                " :reg_date,\n" +
                "(SELECT ID FROM T_USER U WHERE U.USER_KEY=:upt_user_key AND U.ETL_COM_ID=:com_id),\n" +
                ":upt_date,:view_date, :view_end_date, :notice_type, " +
                "(SELECT ID FROM T_COMPANY_CONTENT_CONTRACT CCC WHERE CCC.OLD_TIMELINE_ID=:timeline_idx AND CCC.OWNER_COM_ID=:com_id), " +
                ":editor_detail,:push_flag, :old_id, :com_id)");
    }
    private JdbcBatchItemWriter<TobeNotice> updateAsisWriter() {
        return databaseWriter(asisDataSource, "Update NOTICE SET ETL_STATUS=1 WHERE IDX=:old_id");
    }

    private CompositeItemWriter<TobeNotice> dataBaseWriter() {
        CompositeItemWriter<TobeNotice> compositeItemWriter = new CompositeItemWriter<>();
        compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        return compositeItemWriter;
    }

    @Bean(value = "NoticeInsertJob")
    public Job job1() {
        return jobBuilderFactory.get("NoticeInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(step1())
                .build();
    }

    private Step step1() {
        return stepBuilderFactory.get("NoticeLogInsertStep")
                .<AsisNotice, TobeNotice>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<AsisNotice, TobeNotice>) item -> {
                    log.debug("{}",item);
                    TobeNotice content = TobeNotice.builder()
                            .title(item.getTitle())
                            .reg_user_key(item.getReg_user_key())
                            .reg_date(longToDate(item.getReg_date()))
                            .upt_user_key(item.getUpt_user_key())
                            .upt_date(longToDate(item.getUpt_date()))
                            .view_date(longToDate(item.getView_date()))
                            .view_end_date(longToDate(item.getView_end_date()))
                            .notice_type(item.getNotice_type())
                            .timeline_idx(item.getTimeline_idx())
                            .editor_detail(item.getEditor_detail())
                            .push_flag(item.getPush_flag())
                            .com_id(applicationProperties.getCompanyId())
                            .old_id(item.getIdx())
                            .build();

                    log.debug("{}", content);
                    return content;
                })
                .writer(dataBaseWriter())
                .build();
    }
}
