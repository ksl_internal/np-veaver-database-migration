package com.veaver.etl.notice;

import lombok.Data;

@Data
public class AsisNotice {
    Long idx;
    String title;
    String reg_user_key;
    Long reg_date;
    String upt_user_key;
    Long upt_date;
    Long view_date;
    Long view_end_date;
    String notice_type;
    Long timeline_idx;
    String editor_detail;
    String push_flag;
}
