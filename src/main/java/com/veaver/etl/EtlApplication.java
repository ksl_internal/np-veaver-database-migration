package com.veaver.etl;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;

import java.util.Date;
import java.util.List;

@SpringBootApplication
@EnableConfigurationProperties
@EnableBatchProcessing
public class EtlApplication implements CommandLineRunner{
    public static void main(String[] args) {
        SpringApplication.run(EtlApplication.class, args);
    }

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    JobRepository jobRepository;


    @Value("#{'${spring.batch.job.names:}'.split(',')}")
    private List<String> jobList;

    @Value("${etl.tool.start.batch.jobs:true}")
    private Boolean startBatchJobs;

    @Override
    public void run(String... args) throws Exception {
        if(!startBatchJobs)
            return;
        for(String jobName : jobList) {
            jobName = jobName.trim();
            Job job = (Job) applicationContext.getBean(jobName);
            JobParameters jobParameters = new JobParametersBuilder()
                    .addDate("launchDate", new Date())
                    .toJobParameters();
            JobExecution jobExecution = jobLauncher.run(job, jobParameters);
            System.out.println(jobExecution);
        }
    }
}
