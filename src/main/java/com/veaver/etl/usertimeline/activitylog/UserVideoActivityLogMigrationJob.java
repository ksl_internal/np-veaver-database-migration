package com.veaver.etl.usertimeline.activitylog;

import com.veaver.etl.configuration.BaseJobConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
public class UserVideoActivityLogMigrationJob extends BaseJobConfiguration {

    private static long count =0;

    private JdbcCursorItemReader<AsisUserActionLog> databaseReader() {
        return databaseReader(asisDataSource,
                "SELECT USER_KEY,TIMELINE_IDX,REG_DATE,ACTION_FLAG,PLAY_TIME,ADD_COUNT FROM USER_ACTION_LOG WHERE ETL_STATUS=0 AND TIMELINE_IDX IS NOT NULL",
                AsisUserActionLog.class);
    }

    private JdbcBatchItemWriter<TobeUserVideoActivityLog> insertTobeWriter() {
        return databaseWriter(tobeDataSource,
                "insert INTO T_USER_VIDEO_ACTIVITY_LOG\n" +
                        "(REG_DATE, ACTIVITY_NAME, ACTIVITY_DETAIL, ETL_COM_ID, \n" +
                        " USER_ID,\n" +
                        " COMPANY_CONTENT_CONTRACT_ID)\n" +
                        "VALUES\n" +
                        "(:reg_date,:activity_name,:activity_detail,:com_id,\n" +
                        " (SELECT ID FROM T_USER U WHERE U.USER_KEY=:user_key AND U.ETL_COM_ID=:com_id),\n" +
                        " (SELECT ID FROM T_COMPANY_CONTENT_CONTRACT C WHERE C.OLD_TIMELINE_ID=:old_timeline_id AND C.OWNER_COM_ID=:com_id));");
    }

    private JdbcBatchItemWriter<TobeUserVideoActivityLog> updateAsisWriter() {
        return databaseWriter(asisDataSource, "Update USER_ACTION_LOG SET ETL_STATUS=1 WHERE USER_KEY=:user_key AND TIMELINE_IDX=:old_timeline_id");
    }

    private CompositeItemWriter<TobeUserVideoActivityLog> dataBaseWriter() {
        CompositeItemWriter<TobeUserVideoActivityLog> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }

    @Bean("UserVideoActivityLogInsertJob")
    public Job bookmarkInsertjob() {
        return jobBuilderFactory.get("UserVideoActivityLogInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(userVideoActivityInsertStep())
                .build();
    }

    private Step userVideoActivityInsertStep() {
        return stepBuilderFactory.get("UserVideoActivityLogInsertStep")
                .<AsisUserActionLog, TobeUserVideoActivityLog>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<AsisUserActionLog, TobeUserVideoActivityLog>) item -> {
                    log.debug("{}",item);
                    Map<String,Object> detail_map = new HashMap<>();
                    detail_map.put("play_time", item.getPlay_time());
                    detail_map.put("add_count", item.getAdd_count());
                    TobeUserVideoActivityLog content = TobeUserVideoActivityLog.builder()
                            .com_id(applicationProperties.getCompanyId())
                            .activity_name(item.getAction_flag())
                            .old_timeline_id(item.getTimeline_idx())
                            .user_key(item.getUser_key())
                            .reg_date(longToDate(item.getReg_date()))
                            .activity_detail(objectMapper.writeValueAsString(detail_map))
                            .build();

                    log.debug("{} : {}", count++, content);
                    return content;

                })
                .writer(dataBaseWriter())
                .build();
    }

}
