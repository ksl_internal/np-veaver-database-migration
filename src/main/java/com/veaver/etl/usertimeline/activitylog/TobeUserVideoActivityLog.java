package com.veaver.etl.usertimeline.activitylog;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TobeUserVideoActivityLog {
    private String activity_name;
    private Date reg_date;
    private String activity_detail;
    private String user_key;
    private Long old_timeline_id;
    private String com_id;

}
