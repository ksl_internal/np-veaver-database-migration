package com.veaver.etl.usertimeline.activitylog;

import lombok.Data;

@Data
public class AsisUserActionLog {
    private String user_key;
    private Long timeline_idx;
    private Long reg_date;
    private String action_flag;
    private Long play_time;
    private Long add_count;
}
