package com.veaver.etl.usertimeline.share;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TobeVideoShare {
    private Long old_id;
    private String com_id;
    private Date reg_date;
    private Date upd_date;

    private Long begin_time;
    private Long end_time;
    private String sender_user_key;
    private String receiver_user_key;
    private Integer priority;
    private String message;
    private Long old_timeline_idx;

    private Date approval_date;
    private Long play_time;
    private Date complete_date;
    private Long share_event_id;

    private String status;
}
