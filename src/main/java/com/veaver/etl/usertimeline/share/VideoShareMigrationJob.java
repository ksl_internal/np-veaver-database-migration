package com.veaver.etl.usertimeline.share;

import com.veaver.etl.configuration.BaseJobConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;

@Slf4j
@Configuration
public class VideoShareMigrationJob extends BaseJobConfiguration {
    private JdbcCursorItemReader<AsisVideoShare> databaseReader() {
        return databaseReader(asisDataSource,
                "SELECT \n" +
                        "  S.IDX AS SHARE_IDX,\n" +
                        "  MS.BEGIN AS BEGIN_TIME,\n" +
                        "  MS.END AS END_TIME,\n" +
                        "  MS.IDX AS SHARE_EVENT_ID,\n" +
                        "  MS.USER_KEY AS SENDER_USER_KEY,\n" +
                        "  S.USER_KEY AS RECEIVER_USER_KEY,\n" +
                        "  MS.PRIORITY_IDX AS PRIORITY,\n" +
                        "  MS.HIDDEN_FLAG AS MS_HIDDEN_FLAG,\n" +
                        "  MS.DELETE_FLAG AS MS_DELETE_FLAG,\n" +
                        "  S.HIDDEN_FLAG AS SHARED_HIDDEN_FLAG,\n" +
                        "  S.DELETE_FLAG AS SHARED_DELETE_FLAG,\n" +
                        "  S.APPROVE_STATUS AS APPROVE_STATUS,\n" +
                        "  MS.MESSAGE AS MESSAGE,\n" +
                        "  MS.REG_DATE AS REG_DATE,\n" +
                        "  MS.UPT_DATE AS UPT_DATE,\n" +
                        "  S.TIMELINE_IDX AS TIMELINE_IDX,\n" +
                        "  S.APPROVE_DATE AS APPROVE_DATE,\n" +
                        "  S.PLAY_TIME AS PLAY_TIME,\n" +
                        "  S.COMPLETE_DATE AS COMPLETE_DATE\n" +
                        "FROM MST_SHARE MS INNER JOIN SHARED S ON MS.IDX = S.SHARE_IDX WHERE S.ETL_STATUS=0",
                AsisVideoShare.class);
    }

    private JdbcBatchItemWriter<TobeVideoShare> insertTobeWriter() {

        return databaseWriter(tobeDataSource,
                "INSERT INTO T_USER_VIDEO_SHARE\n" +
                        "(BEGIN_TIME, END_TIME, PRIORITY, MESSAGE, REG_DATE, UPD_DATE, APPROVE_DATE, PLAY_TIME, COMPLETE_DATE, SHARE_EVENT_ID, OLD_ID, ETL_COM_ID, STATUS, \n" +
                        " SENDER_USER_ID,\n" +
                        " RECEIVER_USER_ID, COMPANY_CONTENT_CONTRACT_ID)\n" +
                        "    VALUES\n" +
                        "(:begin_time,:end_time, :priority, :message, :reg_date,:upd_date,:approval_date,:play_time,:complete_date,:share_event_id, :old_id, :com_id, :status, \n" +
                        " (SELECT ID FROM T_USER U WHERE U.USER_KEY=:sender_user_key AND U.ETL_COM_ID=:com_id),\n" +
                        " (SELECT ID FROM T_USER U WHERE U.USER_KEY=:receiver_user_key AND U.ETL_COM_ID=:com_id),\n" +
                        " (SELECT ID FROM T_COMPANY_CONTENT_CONTRACT CCC WHERE CCC.OLD_TIMELINE_ID=:old_timeline_idx AND CCC.OWNER_COM_ID=:com_id)\n" +
                        ")");
    }

    private JdbcBatchItemWriter<TobeVideoShare> updateAsisWriter() {
        return databaseWriter(asisDataSource, "Update SHARED SET ETL_STATUS=1 WHERE IDX=:old_id");
    }

    private CompositeItemWriter<TobeVideoShare> dataBaseWriter() {
        CompositeItemWriter<TobeVideoShare> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }

    @Bean("VideoShareInsertJob")
    public Job bookmarkInsertjob() {
        return jobBuilderFactory.get("VideoShareInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(inviteUserlistCreatorStep())
                .build();
    }

    private Step inviteUserlistCreatorStep() {
        return stepBuilderFactory.get("inviteUserlistCreatorStep")
                .<AsisVideoShare, TobeVideoShare>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<AsisVideoShare, TobeVideoShare>) item -> {
                    log.debug("{}",item);

                    String status=null;
                    if(item.getMs_delete_flag().equals("Y") || item.getShared_delete_flag().equals("Y"))
                        status="DELETED";
                    else if(item.getMs_hidden_flag().equals("Y") || item.getShared_hidden_flag().equals("Y"))
                        status="HIDDEN";
                    else if(item.getApprove_status().equals("W"))
                        status="WAITING";
                    else if(item.getApprove_status().equals("Y"))
                        status="APPROVED";
                    else if(item.getApprove_status().equals("N"))
                        status="REJECTED";

                    TobeVideoShare content = TobeVideoShare.builder()
                            .approval_date(longToDate(item.getApprove_date()))
                            .begin_time(item.getBegin_time())
                            .com_id(applicationProperties.getCompanyId())
                            .end_time(item.getEnd_time())
                            .message(item.getMessage())
                            .old_id(item.getShare_idx())
                            .old_timeline_idx(item.getTimeline_idx())
                            .play_time(item.getPlay_time())
                            .priority(item.getPriority())
                            .receiver_user_key(item.getReceiver_user_key())
                            .reg_date(longToDate(item.getReg_date()))
                            .sender_user_key(item.getSender_user_key())
                            .upd_date(longToDate(item.getUpt_date()))
                            .complete_date(longToDate(item.getComplete_date()))
                            .share_event_id(item.getShare_event_id())
                            .status(status)
                            .build();

                    log.debug("{}", content);
                    return content;

                })
                .writer(dataBaseWriter())
                .build();
    }

}
