package com.veaver.etl.usertimeline.share;

import lombok.Data;

@Data
public class AsisVideoShare {
    private Long share_idx;
    private Long share_event_id;
    private Long begin_time;
    private Long end_time;
    private String sender_user_key;
    private String receiver_user_key;
    private Integer priority;
    private String ms_delete_flag;
    private String shared_delete_flag;
    private String ms_hidden_flag;
    private String shared_hidden_flag;
    private String approve_status;
    private String message;
    private Long reg_date;
    private Long upt_date;
    private Long timeline_idx;
    private Long approve_date;
    private Long play_time;
    private Long complete_date;
}
