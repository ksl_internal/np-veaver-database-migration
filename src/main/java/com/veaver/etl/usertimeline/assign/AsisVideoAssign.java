package com.veaver.etl.usertimeline.assign;

import lombok.Data;

@Data
public class AsisVideoAssign {
    Long idx;
    Long assign_idx;
    String receiver_user_key;
    String sender_user_key;
    Long timeline_idx;
    char assign_status;
    char assigned_delete_flag;
    char mst_assign_delete_flag;
    char hidden_flag;
    Long reg_date;
    Long start_date;
    Long end_date;
    String message;
    Long alarm_time;
    Long complete_date;
    Long played_time;
    String play_section;
}
