package com.veaver.etl.usertimeline.assign;

import com.veaver.etl.configuration.BaseJobConfiguration;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;

@Configuration
public class VideoAssignMigrationJob extends BaseJobConfiguration {
    @Autowired
    VideoAssignProcessor videoAssignProcessor;

    private JdbcCursorItemReader<AsisVideoAssign> databaseReader() {
        return databaseReader(asisDataSource, "SELECT A.IDX, ASSIGN_IDX,A.USER_KEY AS RECEIVER_USER_KEY, M.USER_KEY AS SENDER_USER_KEY,\n" +
                 "A.TIMELINE_IDX,ASSIGN_STATUS,A.DELETE_FLAG AS ASSIGNED_DELETE_FLAG,M.DELETE_FLAG AS MST_ASSIGN_DELETE_FLAG,HIDDEN_FLAG,\n"+
                 "A.REG_DATE,START_DATE,END_DATE,MESSAGE,ALARM_TIME,COMPLETE_DATE,PLAYED_TIME,PLAY_SECTION FROM ASSIGNED AS A INNER JOIN\n"+
                 "MST_ASSIGN AS M ON A.ASSIGN_IDX=M.IDX WHERE ETL_STATUS=0",
                AsisVideoAssign.class);
    }
    private JdbcBatchItemWriter<TobeVideoAssign> insertTobeWriter() {
        return databaseWriter(tobeDataSource, "INSERT INTO T_USER_VIDEO_ASSIGN\n" +
                "(START,END,ALARM_TIME,MESSAGE,STATUS,REG_DATE,\n" +
                "COMPLETE_DATE,PLAYED_TIME,PLAY_SECTION,ASSIGN_EVENT_ID,OLD_ID,ETL_COM_ID," +
                "SENDER_USER_ID, RECEIVER_USER_ID,COMPANY_CONTENT_CONTRACT_ID)\n" +
                "    VALUES\n" +
                "(:start, :end, :alarm_time,:message,:status,:reg_date,\n" +
                ":complete_date,:played_time,:play_section,:assign_event_id,:old_id , :com_id,\n" +
                "(SELECT ID FROM T_USER U WHERE U.USER_KEY=:sender_user_key AND U.ETL_COM_ID=:com_id),\n" +
                "(SELECT ID FROM T_USER U WHERE U.USER_KEY=:receiver_user_key AND U.ETL_COM_ID=:com_id),\n" +
                "(SELECT ID FROM T_COMPANY_CONTENT_CONTRACT CCC WHERE CCC.OLD_TIMELINE_ID=:old_timeline_idx AND CCC.OWNER_COM_ID=:com_id))");
    }
    private JdbcBatchItemWriter<TobeVideoAssign> updateAsisWriter() {
        return databaseWriter(asisDataSource, "UPDATE `ASSIGNED` A SET ETL_STATUS=1 WHERE A.IDX=:old_id");
    }

    private CompositeItemWriter<TobeVideoAssign> dataBaseWriter() {
        CompositeItemWriter<TobeVideoAssign> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }
    @Bean("videoAssignInsertJob")
    public Job videoAssignInsertjob() {
        return jobBuilderFactory.get("videoAssignInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(videoAssignInsertStep())
                .build();
    }

    private Step videoAssignInsertStep() {
        return stepBuilderFactory.get("videoAssignInsertStep")
                .<AsisVideoAssign, TobeVideoAssign>chunk(500)
                .reader(databaseReader())
                .processor(videoAssignProcessor)
                .writer(dataBaseWriter())
                .build();
    }
}
