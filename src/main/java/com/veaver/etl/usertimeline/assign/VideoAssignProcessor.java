package com.veaver.etl.usertimeline.assign;

import com.veaver.etl.configuration.ApplicationProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
@Slf4j
@Component
public class VideoAssignProcessor implements ItemProcessor<AsisVideoAssign,TobeVideoAssign> {
    @Autowired
    ApplicationProperties applicationProperties;
    @Override
    public TobeVideoAssign process(AsisVideoAssign item) throws Exception {
        log.debug("{}",item);
        String status;
        if (item.getAssigned_delete_flag() == 'Y' || item.getMst_assign_delete_flag() == 'Y')
            status = "DELETED";
        else if (item.getHidden_flag() == 'Y')
            status = "HIDDEN";
        else
            status = "ASSIGNED";
        TobeVideoAssign tobeVideoAssign = new TobeVideoAssign();
        tobeVideoAssign.setSender_user_key(item.getSender_user_key());
        tobeVideoAssign.setReceiver_user_key(item.getReceiver_user_key());
        tobeVideoAssign.setOld_id(item.getIdx());
        tobeVideoAssign.setAlarm_time(item.getAlarm_time());
        tobeVideoAssign.setStatus(status);
        tobeVideoAssign.setCom_id(applicationProperties.getCompanyId());
        tobeVideoAssign.setStart(item.getStart_date() == null ? null : new Date(item.getStart_date()));
        tobeVideoAssign.setEnd(item.getEnd_date() == null ? null : new Date(item.getEnd_date()));
        tobeVideoAssign.setMessage(item.getMessage());
        tobeVideoAssign.setReg_date(item.getReg_date() == null ? null : new Date(item.getReg_date()));
        tobeVideoAssign.setComplete_date(item.getComplete_date() == null ? null : new Date(item.getComplete_date()));
        tobeVideoAssign.setPlayed_time(item.getPlayed_time());
        tobeVideoAssign.setPlay_section(item.getPlay_section());
        tobeVideoAssign.setOld_timeline_idx(item.getTimeline_idx());
        tobeVideoAssign.setCompany_content_contract_id(item.getTimeline_idx());
        tobeVideoAssign.setAssign_event_id(item.getAssign_idx());
        return tobeVideoAssign;
    }
}
