package com.veaver.etl.usertimeline.assign;

import lombok.Data;

import java.util.Date;

@Data
public class TobeVideoAssign {
    Long id;
    Date start;
    Date end;
    String sender_user_key;
    String receiver_user_key;
    String status;
    Long alarm_time;
    String message;
    Date reg_date;
    Date complete_date;
    Long played_time;
    String play_section;
    Long old_timeline_idx;
    Long company_content_contract_id;
    Long assign_event_id;
    Long old_id;
    String com_id;
}
