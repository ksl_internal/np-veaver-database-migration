package com.veaver.etl.usertimeline.activity;

import lombok.Data;

@Data
public class AsisUserTimeline {
    private String user_key;
    private Long timeline_idx;
    private Long play_time;
    private String play_section;
    private Long complete_date;
    private Long view_date;
}
