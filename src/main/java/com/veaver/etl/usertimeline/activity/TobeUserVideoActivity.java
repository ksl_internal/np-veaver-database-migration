package com.veaver.etl.usertimeline.activity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TobeUserVideoActivity {
    private Long play_time;
    private String play_section;
    private Date complete_date;
    private Date view_date;
    private Date reg_date;
    private Date upd_date;
    private String user_key;
    private Long old_timeline_id;
    private String com_id;
}
