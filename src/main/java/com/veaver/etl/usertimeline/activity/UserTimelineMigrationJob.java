package com.veaver.etl.usertimeline.activity;

import com.veaver.etl.configuration.BaseJobConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;

@Slf4j
@Configuration
public class UserTimelineMigrationJob extends BaseJobConfiguration {

    private static long count =0;

    private JdbcCursorItemReader<AsisUserTimeline> databaseReader() {
        return databaseReader(asisDataSource,
                "SELECT USER_KEY,TIMELINE_IDX,PLAY_TIME,PLAY_SECTION,COMPLETE_DATE,VIEW_DATE FROM USER_TIMELINE WHERE ETL_STATUS=0",
                AsisUserTimeline.class);
    }

    private JdbcBatchItemWriter<TobeUserVideoActivity> insertTobeWriter() {
        return databaseWriter(tobeDataSource,
                "insert INTO T_USER_VIDEO_ACTIVITY\n" +
                        "(PLAY_TIME, PLAY_SECTION, CMPL_DATE, VIEW_DATE, REG_DATE, ETL_COM_ID,\n" +
                        " USER_ID,\n" +
                        " COMPANY_CONTENT_CONTRACT_ID)\n" +
                        "VALUES\n" +
                        "(:play_time,:play_section,:complete_date,:view_date,:view_date,:com_id,\n" +
                        " (SELECT ID FROM T_USER U WHERE U.USER_KEY=:user_key AND U.ETL_COM_ID=:com_id),\n" +
                        " (SELECT ID FROM T_COMPANY_CONTENT_CONTRACT C WHERE C.OLD_TIMELINE_ID=:old_timeline_id AND C.OWNER_COM_ID=:com_id));");
    }

    private JdbcBatchItemWriter<TobeUserVideoActivity> updateAsisWriter() {
        return databaseWriter(asisDataSource, "Update USER_TIMELINE SET ETL_STATUS=1 WHERE USER_KEY=:user_key AND TIMELINE_IDX=:old_timeline_id");
    }

    private CompositeItemWriter<TobeUserVideoActivity> dataBaseWriter() {
        CompositeItemWriter<TobeUserVideoActivity> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }

    @Bean("UserVideoActivityInsertJob")
    public Job bookmarkInsertjob() {
        return jobBuilderFactory.get("UserVideoActivityInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(userVideoActivityInsertStep())
                .build();
    }

    private Step userVideoActivityInsertStep() {
        return stepBuilderFactory.get("UserVideoActivityInsertStep")
                .<AsisUserTimeline, TobeUserVideoActivity>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<AsisUserTimeline, TobeUserVideoActivity>) item -> {
                    log.debug("{}",item);
                    TobeUserVideoActivity content = TobeUserVideoActivity.builder()
                            .com_id(applicationProperties.getCompanyId())
                            .complete_date(longToDate(item.getComplete_date()))
                            .old_timeline_id(item.getTimeline_idx())
                            .play_section(item.getPlay_section())
                            .play_time(item.getPlay_time())
                            .user_key(item.getUser_key())
                            .view_date(longToDate(item.getView_date()))
                            .build();

                    log.debug("{} : {}", count++, content);
                    return content;

                })
                .writer(dataBaseWriter())
                .build();
    }

}
