package com.veaver.etl.video.content;

import lombok.Data;

import java.util.Date;

@Data
public class MultimediaContent {
    Integer content_id;
    Integer old_id;
    String com_id;
    String file_type;
    String file_url;
    Integer size;
    Integer play_time;
    Integer rsltn_hgt;
    Integer rsltn_wdt;
    String tag;
    Date reg_date;
    Date upd_date;
    String content_type;
    String encoding_status;
    String hls_flag;
    String status;
    Integer multimedia_categ_id;
    Integer old_multimedia_categ_id;
}
