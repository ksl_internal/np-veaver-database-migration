package com.veaver.etl.video.content;

import lombok.Data;

@Data
public class Video {
    Integer idx;
    String file_type;
    String file_url;
    Integer size;
    Integer play_time;
    Integer resolution_height;
    Integer resolution_width;
    String tag;
    Long reg_date;
    Long upt_date;
    String status_flag;
    String delete_flag;
    String hls_flag;
    Integer video_category_idx;
}
