package com.veaver.etl.video.content;

import com.veaver.etl.configuration.BaseJobConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

@Slf4j
@Configuration
public class MultimediaContentMigrationJob extends BaseJobConfiguration {

    private static long count =0;

    private JdbcCursorItemReader<Video> databaseReader() {
        return databaseReader(asisDataSource,
                "select \n" +
                        "    V.IDX as IDX, \n" +
                        "    V.FILE_TYPE AS FILE_TYPE,\n" +
                        "    V.FILE_URL AS FILE_URL,\n" +
                        "    (SELECT VF.SIZE FROM VIDEO_FILE VF WHERE VF.VIDEO_IDX=V.IDX ORDER BY REG_DATE DESC LIMIT 1) AS SIZE,\n" +
                        "    V.PLAY_TIME as PLAY_TIME,\n" +
                        "    V.RESOLUTION_HEIGHT AS RESOLUTION_HEIGHT,\n" +
                        "    V.RESOLUTION_WIDTH AS RESOLUTION_WIDTH,\n" +
                        "    V.TAG AS TAG,\n" +
                        "    V.STATUS_FLAG AS STATUS_FLAG,\n" +
                        "    V.DELETE_FLAG AS DELETE_FLAG,\n" +
                        "    V.HLS_FLAG AS HLS_FLAG,\n" +
                        "    V.REG_DATE AS REG_DATE,\n" +
                        "    V.UPT_DATE AS UPT_DATE," +
                        "    V.VIDEO_CATEGORY_IDX AS VIDEO_CATEGORY_IDX\n" +
                        "from VIDEO V WHERE ETL_STATUS=0",
                Video.class);
    }

    private JdbcBatchItemWriter<MultimediaContent> insertTobeWriter() {
        return databaseWriter(tobeDataSource, "INSERT INTO T_MULTIMEDIA_CONTENT\n" +
                "(OLD_ID, ETL_COM_ID, FILE_TYPE, FILE_URL, SIZE, PLAY_TIME, RSLTN_HGT, RSLTN_WDT, TAG, REG_DATE, UPD_DATE, " +
                "CONTENT_TYPE, ENCODING_STATUS, STATUS, HLS_FLAG, MLTMEDIA_CTG_ID)\n" +
                "    VALUES\n" +
                "(:old_id, :com_id, :file_type, :file_url, :size, :play_time, :rsltn_hgt, :rsltn_wdt, :tag, :reg_date, :upd_date, " +
                ":content_type, :encoding_status, :status, :hls_flag, " +
                "(SELECT ID FROM T_DIVISIONAL_HIERARCHY MC WHERE MC.OLD_ID=:old_multimedia_categ_id AND MC.COM_ID=:com_id) )");
    }

    private JdbcBatchItemWriter<MultimediaContent> updateAsisWriter() {
        return databaseWriter(asisDataSource, "UPDATE VIDEO SET ETL_STATUS=1 WHERE IDX=:old_id");
    }

    private CompositeItemWriter<MultimediaContent> dataBaseWriter() {
        CompositeItemWriter<MultimediaContent> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }


    @Bean(value = "MultimediaContentMigrationJob")
    public Job job1() {
        return jobBuilderFactory.get("multimediaContentInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(step1())
                .build();
    }

    private Step step1() {
        return stepBuilderFactory.get("multimediaContentInsertStep")
                .<Video, MultimediaContent>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<Video, MultimediaContent>) item -> {
                    log.debug("{}",item);

                    String status = null;
                    if(item.delete_flag!=null){
                        if(item.getDelete_flag().equals("Y"))
                            status="DELETED";
                        else status="ACTIVE";
                    }

                    MultimediaContent content = new MultimediaContent();
                    content.setCom_id(applicationProperties.getCompanyId());
                    content.setContent_type("VIDEO");
                    content.setFile_type(item.getFile_type());
                    content.setFile_url(item.getFile_url());
                    content.setOld_id(item.getIdx());
                    content.setOld_multimedia_categ_id(item.getVideo_category_idx());
                    content.setPlay_time(item.getPlay_time());
                    content.setReg_date(new Date(item.getReg_date()));
                    content.setUpd_date(new Date(item.getUpt_date()));
                    content.setRsltn_hgt(item.getResolution_height());
                    content.setRsltn_wdt(item.getResolution_width());
                    content.setSize(item.getSize());
                    content.setTag(item.getTag());
                    content.setEncoding_status(item.getStatus_flag());
                    content.setStatus(status);
                    content.setHls_flag(item.getHls_flag());

                    log.debug("{} : {}", count++, content);
                    return content;
                })
                .writer(dataBaseWriter())
                .build();
    }

}
