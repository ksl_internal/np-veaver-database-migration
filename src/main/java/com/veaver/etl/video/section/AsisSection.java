package com.veaver.etl.video.section;

import lombok.Data;

@Data
public class AsisSection {
    private Long idx;
    private Long timeline_idx;
    private Long reg_date;
    private Integer begin;
    private String delete_flag;
    private String name;
}
