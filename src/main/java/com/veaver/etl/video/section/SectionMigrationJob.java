package com.veaver.etl.video.section;

import com.veaver.etl.configuration.BaseJobConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;

@Slf4j
@Configuration
public class SectionMigrationJob extends BaseJobConfiguration {

    private static long count =0;

    private JdbcCursorItemReader<AsisSection> databaseReader() {
        return databaseReader(asisDataSource,
                "SELECT " +
                        "IDX, TIMELINE_IDX, BEGIN, NAME, DELETE_FLAG, " +
                        "(SELECT T.REG_DATE FROM TIMELINE T WHERE T.IDX=TIMELINE_IDX) AS REG_DATE " +
                        "FROM SECTION WHERE ETL_STATUS=0",
                AsisSection.class);
    }

    private JdbcBatchItemWriter<TobeSection> insertTobeWriter() {
        return databaseWriter(tobeDataSource, "INSERT INTO T_SECTION\n" +
                "(OLD_ID, ETL_COM_ID, SEC_NAME, STATUS, BEGIN, REG_DATE, TIMELINE_ID)\n" +
                "VALUES\n" +
                "(:old_id, :com_id, :sec_name, :status, :begin, :reg_date," +
                "(SELECT ID FROM T_MULTIMEDIA_TIMELINE MT WHERE MT.OLD_ID=:old_timeline_idx AND MT.ETL_COM_ID=:com_id))");
    }


    private JdbcBatchItemWriter<TobeSection> updateAsisWriter() {
        return databaseWriter(asisDataSource, "Update SECTION SET ETL_STATUS=1 WHERE IDX=:old_id");
    }

    private CompositeItemWriter<TobeSection> dataBaseWriter() {
        CompositeItemWriter<TobeSection> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }

    @Bean(value = "SectionInsertJob")
    public Job job1() {
        return jobBuilderFactory.get("SectionInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(step1())
                .build();
    }

    private Step step1() {
        return stepBuilderFactory.get("SectionInsertStep")
                .<AsisSection, TobeSection>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<AsisSection, TobeSection>) item -> {
                    log.debug("{}",item);

                    String status="ACTIVE";
                    if(item.getDelete_flag().equals("Y"))
                        status="DELETED";

                    TobeSection content = TobeSection.builder()
                            .begin(item.getBegin())
                            .com_id(applicationProperties.getCompanyId())
                            .old_id(item.getIdx())
                            .old_timeline_idx(item.getTimeline_idx())
                            .sec_name(item.getName())
                            .status(status)
                            .reg_date(longToDate(item.getReg_date()))
                            .build();

                    log.debug("{} : {}", count++, content);
                    return content;
                })
                .writer(dataBaseWriter())
                .build();
    }
}
