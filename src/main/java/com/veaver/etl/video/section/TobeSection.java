package com.veaver.etl.video.section;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TobeSection {
    private Long old_id;
    private String com_id;
    private Long old_timeline_idx;
    private String sec_name;
    private String status;
    private Integer begin;
    private Date reg_date;
}
