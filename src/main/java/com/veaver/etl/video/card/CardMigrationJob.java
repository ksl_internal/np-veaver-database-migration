package com.veaver.etl.video.card;

import com.veaver.etl.configuration.BaseJobConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;

@Slf4j
@Configuration
public class CardMigrationJob extends BaseJobConfiguration {

    private static long count =0;

    private JdbcCursorItemReader<AsisCard> databaseReader() {
        return databaseReader(asisDataSource,
                "SELECT C.IDX, C.STACK_IDX, C.TYPE, C.META, C.DELETE_FLAG, C.REG_DATE, C.UPT_DATE, C.ORDER_VALUE FROM CARD C INNER JOIN STACK S " +
                        " ON C.STACK_IDX=S.IDX" +
                        " WHERE C.ETL_STATUS=0 AND `BEGIN` >= 0 AND `BEGIN` <=  `END`",
                AsisCard.class);
    }

    private JdbcBatchItemWriter<TobeCard> insertTobeWriter() {
        return databaseWriter(tobeDataSource, "INSERT INTO T_CARD\n" +
                "(OLD_ID, ETL_COM_ID, TYPE, METADATA, REG_DATE, UPD_DATE, CARD_ORDER, STATUS, STACK_ID)\n" +
                "VALUES\n" +
                "(:old_id, :com_id, :type, :metadata, :reg_date, :upd_date, :card_order, :status,   " +
                "(SELECT ID FROM T_STACK S WHERE S.OLD_ID=:old_stack_id AND S.ETL_COM_ID=:com_id))");
    }

    private JdbcBatchItemWriter<TobeCard> updateAsisWriter() {
        return databaseWriter(asisDataSource, "Update CARD SET ETL_STATUS=1 WHERE IDX=:old_id");
    }

    private CompositeItemWriter<TobeCard> dataBaseWriter() {
        CompositeItemWriter<TobeCard> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }

    @Bean(value = "CardInsertJob")
    public Job job1() {
        return jobBuilderFactory.get("CardInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(step1())
                .build();
    }

    private Step step1() {
        return stepBuilderFactory.get("CardInsertStep")
                .<AsisCard, TobeCard>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<AsisCard, TobeCard>) item -> {
                    log.debug("{}",item);

                    String status="ACTIVE";
                    if(item.getDelete_flag().equals("Y"))
                        status="DELETED";

                    TobeCard content = TobeCard.builder()
                            .card_order(item.getOrder_value())
                            .com_id(applicationProperties.getCompanyId())
                            .metadata(item.getMeta())
                            .old_id(item.getIdx())
                            .old_stack_id(item.getStack_idx())
                            .reg_date(longToDate(item.getReg_date()))
                            .type(item.getType())
                            .upd_date(longToDate(item.getUpt_date()))
                            .status(status)
                            .build();

                    log.debug("{} : {}", count++, content);
                    return content;
                })
                .writer(dataBaseWriter())
                .build();
    }
}
