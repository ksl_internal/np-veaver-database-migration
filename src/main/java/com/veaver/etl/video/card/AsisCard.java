package com.veaver.etl.video.card;

import lombok.Data;

@Data
public class AsisCard {
    private Long idx;
    private Long stack_idx;
    private String type;
    private String meta;
    private String delete_flag;
    private Long reg_date;
    private Long upt_date;
    private Integer order_value;
}
