package com.veaver.etl.video.card;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TobeCard {
    private Long old_id;
    private String com_id;
    private String name;
    private String type;
    private String metadata;
    private Date reg_date;
    private Date upd_date;
    private Long old_stack_id;
    private Integer card_order;
    private String status;
}
