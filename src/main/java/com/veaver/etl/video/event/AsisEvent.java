package com.veaver.etl.video.event;

import lombok.Data;

@Data
public class AsisEvent {
    private Long idx;
    private Long timeline_idx;
    private String type;
    private Integer begin;
    private String meta;
    private Long reg_date;
}
