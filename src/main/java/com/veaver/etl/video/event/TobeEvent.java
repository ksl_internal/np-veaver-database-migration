package com.veaver.etl.video.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TobeEvent {
    private Long old_id;
    private String com_id;
    private String type;
    private String meta;
    private Integer begin;
    private Long old_timeline_idx;
    private Date reg_date;
}
