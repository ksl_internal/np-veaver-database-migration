package com.veaver.etl.video.event;

import com.veaver.etl.configuration.BaseJobConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;

@Slf4j
@Configuration
public class EventMigrationJob extends BaseJobConfiguration {

    private static long count =0;

    private JdbcCursorItemReader<AsisEvent> databaseReader() {
        return databaseReader(asisDataSource,
                "SELECT IDX, TIMELINE_IDX, TYPE, META, BEGIN, " +
                        "(SELECT T.REG_DATE FROM TIMELINE T WHERE T.IDX=TIMELINE_IDX) AS REG_DATE " +
                        " FROM EVENT WHERE ETL_STATUS=0;",
                AsisEvent.class);
    }

    private JdbcBatchItemWriter<TobeEvent> insertTobeWriter() {
        return databaseWriter(tobeDataSource, "INSERT INTO T_EVENT\n" +
                "(OLD_ID, ETL_COM_ID, TYPE, META, BEGIN, REG_DATE, TIMELINE_ID)\n" +
                "VALUES\n" +
                "(:old_id, :com_id, :type, :meta, :begin, :reg_date,  " +
                "(SELECT ID FROM T_MULTIMEDIA_TIMELINE S WHERE S.OLD_ID=:old_timeline_idx AND S.ETL_COM_ID=:com_id))");
    }

    private JdbcBatchItemWriter<TobeEvent> updateAsisWriter() {
        return databaseWriter(asisDataSource, "Update EVENT SET ETL_STATUS=1 WHERE IDX=:old_id");
    }

    private CompositeItemWriter<TobeEvent> dataBaseWriter() {
        CompositeItemWriter<TobeEvent> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }

    @Bean(value = "EventInsertJob")
    public Job job1() {
        return jobBuilderFactory.get("EventInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(step1())
                .build();
    }

    private Step step1() {
        return stepBuilderFactory.get("EventInsertStep")
                .<AsisEvent, TobeEvent>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<AsisEvent, TobeEvent>) item -> {
                    log.debug("{}",item);
                    TobeEvent content = TobeEvent.builder()
                            .begin(item.getBegin())
                            .meta(item.getMeta())
                            .old_id(item.getIdx())
                            .type(item.getType())
                            .old_timeline_idx(item.getTimeline_idx())
                            .com_id(applicationProperties.getCompanyId())
                            .reg_date(longToDate(item.getReg_date()))
                            .build();

                    log.debug("{} : {}", count++, content);
                    return content;
                })
                .writer(dataBaseWriter())
                .build();
    }
}
