package com.veaver.etl.video.timeline;

import lombok.Data;

@Data
public class AsisTimeline {
    private Long idx;
    private String title;
    private String description;
    private String thumbnail;
    private String tag;
    private Long reg_date;
    private Long upt_date;
    private Long publish_date;
    private String user_key;
    private String name;
    private Long video_idx;
    private String public_flag;
    private String delete_flag;
    private String hidden_flag;
    private String status_flag;
}
