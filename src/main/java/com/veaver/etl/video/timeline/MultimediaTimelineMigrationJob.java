package com.veaver.etl.video.timeline;

import com.veaver.etl.configuration.BaseJobConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;

@Slf4j
@Configuration
public class MultimediaTimelineMigrationJob extends BaseJobConfiguration {

    private static long count =0;

    private JdbcCursorItemReader<AsisTimeline> databaseReader() {
        return databaseReader(asisDataSource,
                "SELECT \n" +
                        "    T.IDX,\n" +
                        "    V.TITLE AS TITLE,\n" +
                        "    T.DESCRIPTION AS DESCRIPTION,\n" +
                        "    T.THUMBNAIL AS THUMBNAIL,\n" +
                        "    T.TAG AS TAG,\n" +
                        "    T.REG_DATE AS REG_DATE,\n" +
                        "    T.UPT_DATE AS UPT_DATE,\n" +
                        "    T.PUBLISH_DATE AS PUBLISH_DATE,\n" +
                        "    T.PUBLIC_FLAG AS PUBLIC_FLAG,\n" +
                        "    T.USER_KEY AS USER_KEY,\n" +
                        "    T.DELETE_FLAG AS DELETE_FLAG,\n" +
                        "    T.HIDDEN_FLAG AS HIDDEN_FLAG,\n" +
                        "    T.STATUS_FLAG AS STATUS_FLAG,\n" +
                        "    T.NAME AS NAME,\n" +
                        "    T.VIDEO_IDX AS VIDEO_IDX\n" +
                        "FROM\n" +
                        "TIMELINE T LEFT JOIN VIDEO V \n" +
                        "    ON T.VIDEO_IDX = V.IDX" +
                        "    WHERE T.ETL_STATUS=0;",
                AsisTimeline.class);
    }

    private JdbcBatchItemWriter<MultimediaTimeline> insertTobeWriter() {
        return databaseWriter(tobeDataSource, "INSERT INTO T_MULTIMEDIA_TIMELINE \n" +
                "(OLD_ID,   ETL_COM_ID, TITLE, DESCRIPTION, TMBNL, TAG , REG_DATE, UPD_DATE, PUBLISH_DATE, PRODUCER_COM_ID, NAME, PRIVACY, STATUS, " +
                "PRODUCER_USER_ID," +
                "MLTMEDIA_CONTENT_ID) \n" +
                "VALUES \n" +
                "(:old_id, :com_id,:title,:description,:tmbnl,:tag ,:reg_date,:upd_date,:publish_date,:producer_com_id,:name,:privacy,:status," +
                "(SELECT ID FROM T_USER U WHERE U.USER_KEY=:user_key AND U.ETL_COM_ID=:com_id), " +
                "(SELECT ID FROM T_MULTIMEDIA_CONTENT MC WHERE MC.OLD_ID=:video_idx AND MC.ETL_COM_ID=:com_id));");
    }

    private JdbcBatchItemWriter<MultimediaTimeline> updateAsisWriter() {
        return databaseWriter(asisDataSource, "Update TIMELINE SET ETL_STATUS=1 WHERE IDX=:old_id");
    }

    private CompositeItemWriter<MultimediaTimeline> dataBaseWriter() {
        CompositeItemWriter<MultimediaTimeline> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }



    @Bean(value = "MultimediaTimelineMigrationJob")
    public Job job1() {
        return jobBuilderFactory.get("multimediaTimelineInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(step1())
                .build();
    }

    private Step step1() {
        return stepBuilderFactory.get("multimediaTimelineInsertStep")
                .<AsisTimeline, MultimediaTimeline>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<AsisTimeline, MultimediaTimeline>) item -> {
                    log.debug("{}",item);

                    String status = null;
                    if(item.getDelete_flag().equals("Y"))
                        status="DELETED";
                    else if(item.getHidden_flag().equals("Y"))
                        status="HIDDEN";
                    else if(item.getStatus_flag().equals("C"))
                        status="COMPLETE";
                    else if(item.getStatus_flag().equals("R"))
                        status="READY";

                    MultimediaTimeline result = MultimediaTimeline.builder()
                            .com_id(applicationProperties.getCompanyId())
                            .com_role_cd(applicationProperties.getCompanyRoleCode())
                            .description(item.getDescription())
                            .name(item.getName())
                            .old_id(item.getIdx())
                            .producer_com_id(applicationProperties.getCompanyId())
                            .publish_date(longToDate(item.getPublish_date()))
                            .reg_date(longToDate(item.getReg_date()))
                            .tag(item.getTag())
                            .title(item.getTitle())
                            .tmbnl(item.getThumbnail())
                            .upd_date(longToDate(item.getUpt_date()))
                            .user_key(item.getUser_key())
                            .video_idx(item.getVideo_idx())
                            .privacy(item.getPublic_flag())
                            .status(status)
                            .build();
                    log.debug("{} : {}", count++, result);
                    return result;
                })
                .writer(dataBaseWriter())
                .build();
    }
}
