package com.veaver.etl.video.timeline;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MultimediaTimeline {
    private String timeline_id;
    private Long old_id;
    private String com_id;
    private String title;
    private String description;
    private String tmbnl;
    private String tag;
    private Date reg_date;
    private Date upd_date;
    private Date publish_date;
    private String producer_com_id;
    private String privacy;
    private String name;
    private String com_role_cd;

    private String user_key;
    private Long video_idx;
    private String status;
}
