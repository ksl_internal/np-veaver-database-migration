package com.veaver.etl.video.stack;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TobeStack {
    private Long old_id;
    private String com_id;
    private Integer begin;
    private Integer end;
    private String name;
    private String status;
    private Long section_id;
    private Date reg_date;
}
