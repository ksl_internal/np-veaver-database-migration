package com.veaver.etl.video.stack;

import lombok.Data;

@Data
public class AsisStack {
    private Long idx;
    private Integer begin;
    private Integer end;
    private String name;
    private String tag;
    private String active_flag;
    private String delete_flag;
    private Long section_id;
    private Integer section_end;
    private Long reg_date;
}
