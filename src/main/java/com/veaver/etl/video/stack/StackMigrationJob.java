package com.veaver.etl.video.stack;

import com.veaver.etl.configuration.BaseJobConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;

@Slf4j
@Configuration
public class StackMigrationJob extends BaseJobConfiguration {

    private static long count =0;

    private JdbcCursorItemReader<AsisStack> databaseReader() {
        return databaseReader(asisDataSource,
                "select IDX, `BEGIN`, `END` , NAME, TAG, ACTIVE_FLAG, DELETE_FLAG," +
                        "(SELECT T.REG_DATE FROM TIMELINE T WHERE T.IDX=TIMELINE_IDX) AS REG_DATE ,\n" +
                        "(    SELECT \n" +
                        "        SC.IDX \n" +
                        "    FROM TIMELINE T  INNER JOIN SECTION SC ON T.IDX=SC.TIMELINE_IDX \n" +
                        "    WHERE T.IDX=S.TIMELINE_IDX AND SC.`BEGIN` <= S.`BEGIN` \n" +
                        "    ORDER BY SC.`BEGIN` DESC LIMIT 1\n" +
                        ") AS SECTION_ID,\n" +
                        "( \n" +
                        "    SELECT SC1.`BEGIN` \n" +
                        "    FROM TIMELINE T  INNER JOIN SECTION SC1 ON T.IDX=SC1.TIMELINE_IDX \n" +
                        "    WHERE T.IDX=S.TIMELINE_IDX AND SC1.`BEGIN` > S.`BEGIN` \n" +
                        "    ORDER BY SC1.`BEGIN` LIMIT 1\n" +
                        ") AS SECTION_END\n" +
                        "from STACK S WHERE ETL_STATUS=0 AND `BEGIN` >= 0 AND `BEGIN` <=  `END`;\n" +
                        "\n",
                AsisStack.class);
    }

    private JdbcBatchItemWriter<TobeStack> insertTobeWriter() {
        return databaseWriter(tobeDataSource,
                "INSERT INTO T_STACK " +
                        "(OLD_ID, ETL_COM_ID, BEGIN, END, NAME, STATUS, REG_DATE, SECTION_ID) " +
                        "VALUES " +
                        "(:old_id, :com_id, :begin, :end, :name, :status, :reg_date, (select ID FROM T_SECTION S WHERE S.OLD_ID=:section_id AND S.ETL_COM_ID=:com_id))");
    }

    private JdbcBatchItemWriter<TobeStack> updateAsisWriter() {
        return databaseWriter(asisDataSource, "Update STACK SET ETL_STATUS=1 WHERE IDX=:old_id");
    }

    private CompositeItemWriter<TobeStack> dataBaseWriter() {
        CompositeItemWriter<TobeStack> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }

    @Bean(value = "StackInsertJob")
    public Job job1() {
        return jobBuilderFactory.get("StackInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(step1())
                .build();
    }

    private Step step1() {
        return stepBuilderFactory.get("StackInsertStep")
                .<AsisStack, TobeStack>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<AsisStack, TobeStack>) item -> {
                    log.debug("{}",item);
                    String status = "ACTIVE";
                    if(item.getDelete_flag().equals("Y"))
                        status="DELETED";
                    else if(item.getActive_flag().equals("N"))
                        status="INACTIVE";
                    TobeStack content = TobeStack.builder()
                            .begin(item.getBegin())
                            .com_id(applicationProperties.getCompanyId())
                            .end((item.getSection_end() == null || item.getSection_end() > item.getEnd() )? item.getEnd() : item.getSection_end())
                            .name(item.getName())
                            .old_id(item.getIdx())
                            .section_id(item.getSection_id())
                            .status(status)
                            .reg_date(longToDate(item.getReg_date()))
                            .build();

                    log.debug("{} : {}", count++, content);
                    return content;
                })
                .writer(dataBaseWriter())
                .build();
    }
}
