package com.veaver.etl.video.contract;

import lombok.Data;

@Data
public class AsisTimelineReaction {
    private Long idx;
    private Integer like_count;
    private Integer comment_count;
    private Integer share_count;
    private Integer view_count;
    private Long reg_date;
    private Long upt_date;
}
