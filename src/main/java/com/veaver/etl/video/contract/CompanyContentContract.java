package com.veaver.etl.video.contract;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompanyContentContract {
    private String com_id;
    private String com_role_cd;
    private Long old_timeline_id;
    private Date reg_date;
    private Date upd_date;
    private Integer like_count;
    private Integer comment_count;
    private Integer share_count;
    private Integer view_count;
}
