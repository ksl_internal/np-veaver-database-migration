package com.veaver.etl.video.contract;

import com.veaver.etl.configuration.BaseJobConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;

@Slf4j
@Configuration
public class CompanyContentContractMigrationJob extends BaseJobConfiguration {

    private static long count =0;

    private JdbcCursorItemReader<AsisTimelineReaction> databaseReader() {
        return databaseReader(asisDataSource,
                "select \n" +
                    "    IDX,\n" +
                    "    LIKE_COUNT,\n" +
                    "    COMMENT_COUNT,\n" +
                    "    SHARE_COUNT,\n" +
                    "    VIEW_COUNT,\n" +
                    "    REG_DATE,\n" +
                    "    UPT_DATE\n" +
                    "FROM TIMELINE WHERE ETL_STATUS_2=0;",
                AsisTimelineReaction.class);
    }

    private JdbcBatchItemWriter<CompanyContentContract> insertTobeWriter() {
        return databaseWriter(tobeDataSource, "INSERT  INTO T_COMPANY_CONTENT_CONTRACT\n" +
                "( REG_DATE, UPD_DATE,  VIEW_CNT, CMNT_CNT, LIKE_CNT, SHARE_CNT, OWNER_COM_ID, ETL_COM_ID, \n" +
                "  OLD_TIMELINE_ID, TIMELINE_ID)\n" +
                "VALUES\n" +
                "(:reg_date, :upd_date, :view_count, :comment_count, :like_count, :share_count, :com_id,  :com_id, \n" +
                " :old_timeline_id , (SELECT ID FROM T_MULTIMEDIA_TIMELINE T WHERE T.OLD_ID = :old_timeline_id AND T.ETL_COM_ID=:com_id))");
    }

    private JdbcBatchItemWriter<CompanyContentContract> updateAsisWriter() {
        return databaseWriter(asisDataSource, "Update TIMELINE SET ETL_STATUS_2=1 WHERE IDX=:old_timeline_id");
    }

    private CompositeItemWriter<CompanyContentContract> dataBaseWriter() {
        CompositeItemWriter<CompanyContentContract> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }

    @Bean(value = "CompanyContentContractInsertJob")
    public Job job1() {
        return jobBuilderFactory.get("CompanyContentContractInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(step1())
                .build();
    }

    private Step step1() {
        return stepBuilderFactory.get("CompanyContentContractInsertStep")
                .<AsisTimelineReaction, CompanyContentContract>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<AsisTimelineReaction, CompanyContentContract>) item -> {
                    log.debug("{}",item);
                    CompanyContentContract result = CompanyContentContract.builder()
                            .com_id(applicationProperties.getCompanyId())
                            .comment_count(item.getComment_count())
                            .like_count(item.getLike_count())
                            .old_timeline_id(item.getIdx())
                            .reg_date(longToDate(item.getReg_date()))
                            .share_count(item.getShare_count())
                            .upd_date(longToDate(item.getUpt_date()))
                            .view_count(item.getView_count())
                            .build();
                    log.debug("{} : {}", count++, result);
                    return result;
                })
                .writer(dataBaseWriter())
                .build();
    }
}
