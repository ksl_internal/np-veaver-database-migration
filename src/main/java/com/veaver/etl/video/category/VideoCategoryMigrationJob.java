package com.veaver.etl.video.category;

import com.veaver.etl.common.DivisionalHierarchy;
import com.veaver.etl.configuration.BaseJobConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.*;

@Slf4j
@Configuration
public class VideoCategoryMigrationJob extends BaseJobConfiguration {

    private static long count =0;

    private JdbcCursorItemReader<ViewVideoCategory> databaseReader() {
        return databaseReader(asisDataSource, "SELECT IDX , NAME , REG_DATE, UPT_DATE, SEQUENCE, DEPTH, PARENT_IDX, TAG FROM VIEW_VIDEO_CATEGORY WHERE ETL_STATUS=0",
                ViewVideoCategory.class);
    }

    private JdbcBatchItemWriter<DivisionalHierarchy> insertTobeWriter() {
        return databaseWriter(tobeDataSource, "INSERT INTO T_DIVISIONAL_HIERARCHY\n" +
                "(OLD_ID, NAME, TYPE, OLD_PARENT_ID, REG_DATE, UPD_DATE, ADDITIONAL_DATA, LEVEL, COM_ID)\n" +
                "    VALUES\n" +
                "(:old_id, :name, :type, :old_parent_id, :reg_date, :upd_date, :additionalData, :level, :com_id)");
    }

    private JdbcBatchItemWriter<DivisionalHierarchy> updateAsisWriter() {
        return databaseWriter(asisDataSource, "UPDATE VIEW_VIDEO_CATEGORY SET ETL_STATUS=1 WHERE IDX=:old_id");
    }

    private CompositeItemWriter<DivisionalHierarchy> dataBaseWriter() {
        CompositeItemWriter<DivisionalHierarchy> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }


    @Bean(value = "VideoCategoryMigrationJob")
    public Job job1() {
        return jobBuilderFactory.get("multimediaCategoryInsertJob")
                .incrementer(new RunIdIncrementer())
                .start(step1())
                .next(step2())
                .build();
    }

    private Step step1() {
        return stepBuilderFactory.get("multimediaCategoryInsertStep")
                .<ViewVideoCategory, DivisionalHierarchy>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<ViewVideoCategory, DivisionalHierarchy>) item -> {

                    Map<String, Object> datamap = new HashMap<>();
                    datamap.put("depth",item.getDepth());
                    datamap.put("sequence", item.getSequence());
                    datamap.put("tag_list",item.getTag());

                    log.debug("{}",item);
                    DivisionalHierarchy divisionalHierarchy = new DivisionalHierarchy();
                    divisionalHierarchy.setCom_id(applicationProperties.getCompanyId());
                    divisionalHierarchy.setName(item.getName());
                    divisionalHierarchy.setType("VIDEO_CATEGORY");
                    divisionalHierarchy.setLevel(item.getDepth());
                    divisionalHierarchy.setAdditionalData(objectMapper.writeValueAsString(datamap));
                    divisionalHierarchy.setOld_id(item.getIdx());
                    divisionalHierarchy.setOld_parent_id(item.getParent_idx());
                    divisionalHierarchy.setReg_date(new Date(item.getReg_date()));
                    divisionalHierarchy.setUpd_date(new Date(item.getUpt_date()));

                    log.debug("{} : {}", count++, divisionalHierarchy);
                    return divisionalHierarchy;
                })
                .writer(dataBaseWriter())
                .build();
    }

    private Step step2() {
        return stepBuilderFactory.get("parentCategoryIdInsertStep")
                .tasklet((contribution, chunkContext) -> {
                    jdbcTemplate.update("UPDATE T_DIVISIONAL_HIERARCHY AS M1 SET M1.PARENT_ID = ( " +
                            "SELECT ID FROM (" +
                            "            SELECT * FROM T_DIVISIONAL_HIERARCHY M3 WHERE M3.COM_ID='"+applicationProperties.getCompanyId()+"') " +
                            "   AS M2 WHERE M2.OLD_ID = M1.OLD_PARENT_ID ) " +
                            "WHERE M1.OLD_PARENT_ID IS NOT NULL AND M1.PARENT_ID IS NULL ");
                    return RepeatStatus.FINISHED;
                })
                .build();
    }
}
