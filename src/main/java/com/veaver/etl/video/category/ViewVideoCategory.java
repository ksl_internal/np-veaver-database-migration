package com.veaver.etl.video.category;

import lombok.Data;

@Data
public class ViewVideoCategory {
    Integer idx;
    String name;
    Long reg_date;
    Long upt_date;
    Integer sequence;
    Integer depth;
    Integer parent_idx;
    String tag;
}
