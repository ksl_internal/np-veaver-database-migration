package com.veaver.etl.playlist.inviteuserlist;

import lombok.Data;

import java.util.Date;

@Data
public class InviteUserList {
    Long idx;
    Long folder_idx;
    String folder_name;
    String user_key;
    String invite_user_key;
    String message;
    String approve_status;
    String delete_flag;
    Long approve_date;
    Long upt_date;
    Long reg_date;
    String com_id;

    Date regDate;
    Date updDate;
    Date approveDate;

    String status;

}
