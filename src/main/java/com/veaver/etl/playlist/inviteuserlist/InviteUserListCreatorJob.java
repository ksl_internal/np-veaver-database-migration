package com.veaver.etl.playlist.inviteuserlist;

import com.veaver.etl.configuration.BaseJobConfiguration;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;

@Configuration
public class InviteUserListCreatorJob extends BaseJobConfiguration {

    private static long count =0;

    private JdbcCursorItemReader<InviteUserList> databaseReader() {
        return databaseReader(asisDataSource, "SELECT IDX, FOLDER_IDX, FOLDER_NAME, " +
                "USER_KEY, INVITE_USER_KEY, MESSAGE, APPROVE_STATUS, DELETE_FLAG, APPROVE_DATE, UPT_DATE, REG_DATE\n" +
                "FROM INVITE_GROUP_FOLDER WHERE ETL_STATUS=0", InviteUserList.class);
    }

    private JdbcBatchItemWriter<InviteUserList> insertTobeWriter() {

        return databaseWriter(tobeDataSource, "INSERT INTO T_INVITE_USER_LIST " +
                "(PLAY_LIST_ID, USER_ID, STATUS, APPROVE_DATE,REG_DATE,  UPD_DATE, MESSAGE, OLD_ID, ETL_COM_ID)" +
                "VALUES((select ID from T_PLAYLIST P where P.OLD_ID=:folder_idx AND P.ETL_COM_ID=:com_id)," +
                "  (SELECT ID FROM T_USER U WHERE U.USER_KEY=:user_key AND U.ETL_COM_ID=:com_id),"+
                "  :status,:approveDate, :regDate, :updDate, :message, :idx,:com_id)");
    }

    private JdbcBatchItemWriter<InviteUserList> updateAsisWriter() {
        return databaseWriter(asisDataSource, "Update INVITE_GROUP_FOLDER SET ETL_STATUS=1 WHERE IDX=:idx");
    }

    private CompositeItemWriter<InviteUserList> dataBaseWriter() {
        CompositeItemWriter<InviteUserList> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }

    private Step inviteUserlistCreatorStep() {
        return stepBuilderFactory.get("inviteUserlistCreatorStep")
                .<InviteUserList, InviteUserList>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<InviteUserList, InviteUserList>) item -> {

                    String status = null;
                    if(item.getDelete_flag().equals("Y"))
                        status="DELETED";
                    else if(item.getApprove_status().equals("W"))
                        status="WAITING";
                    else if(item.getApprove_status().equals("N"))
                        status="REJECTED";
                    else if(item.getApprove_status().equals("Y"))
                        status="APPROVED";

                    item.setRegDate(item.getReg_date()==null?null:longToDate(item.getReg_date()));
                    item.setUpdDate(item.getUpt_date()==null?null:longToDate(item.getUpt_date()));
                    item.setApproveDate(item.getApprove_date()==null?null:longToDate(item.getApprove_date()));
                    item.setCom_id(applicationProperties.getCompanyId());
                    item.setStatus(status);
                    return item;
                })
                .writer(dataBaseWriter())
                .build();
    }


    @Bean("inviteUserListInsertjob")
    public Job bookmarkInsertjob() {
        return jobBuilderFactory.get("inviteUserListInsertjob")
                .incrementer(new RunIdIncrementer())
                .start(inviteUserlistCreatorStep())
                .build();
    }


}
