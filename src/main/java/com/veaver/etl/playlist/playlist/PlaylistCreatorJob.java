package com.veaver.etl.playlist.playlist;

import com.veaver.etl.configuration.BaseJobConfiguration;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;

@Configuration
public class PlaylistCreatorJob extends BaseJobConfiguration {

    private static long count =0;

    private JdbcCursorItemReader<Playlist> databaseReader() {
        return databaseReader(asisDataSource, "SELECT FO.IDX, FOLDER_NAME, FOLDER_TYPE_IDX, " +
                "USER_KEY, PIN_COUNT, THUMBNAIL_TIMELINE_IDX, RECENT_THUMBNAIL, DELETE_FLAG, " +
                "LAST_ADD_PIN_DATE, UPT_DATE, REG_DATE, MFT.FOLDER_TYPE_NAME\n" +
                "FROM FOLDER FO INNER JOIN MST_FOLDER_TYPE MFT ON  FO.FOLDER_TYPE_IDX=MFT.IDX WHERE FO.ETL_STATUS=0" , Playlist.class);
    }

    private JdbcBatchItemWriter<Playlist> insertTobeWriter() {

        return databaseWriter(tobeDataSource, "INSERT INTO T_PLAYLIST\n" +
                "(PLAYLIST_NAME, RECENT_TMBNL, TYPE, STATUS, REG_DATE, UPD_DATE, " +
                "USER_ID, TMBNL_CONTENT_CONTRACT_ID, LAST_VIDEO_ADDED, VIDEO_COUNT, OLD_ID, ETL_COM_ID)\n" +
                "VALUES(:folder_name, :recent_thumbnail, :folder_type_name, :status, :regDate , :updDate, " +
                "(SELECT ID FROM T_USER U WHERE U.USER_KEY=:user_key AND U.ETL_COM_ID=:com_id), " +
                "(select ID from T_COMPANY_CONTENT_CONTRACT CCC where CCC.OLD_TIMELINE_ID=:thumbnail_timeline_idx " +
                "AND CCC.OWNER_COM_ID=:com_id), " +
                ":lastVideoAddDate, :pin_count, :idx, :com_id);");
    }


    private JdbcBatchItemWriter<Playlist> updateAsisWriter() {
        return databaseWriter(asisDataSource, "Update FOLDER SET ETL_STATUS=1 WHERE IDX=:idx");
    }

    private CompositeItemWriter<Playlist> dataBaseWriter() {
        CompositeItemWriter<Playlist> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }

    private Step playlistCreatorStep() {
        return stepBuilderFactory.get("playlistCreatorStep")
                .<Playlist, Playlist>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<Playlist, Playlist>) item -> {

                    String status="ACTIVE";
                    if(item.getDelete_flag().equals("Y"))
                        status="DELETED";

                    item.setRegDate(longToDate(item.getReg_date()));
                    item.setUpdDate(longToDate(item.getUpt_date()));
                    item.setLastVideoAddDate(longToDate(item.getLast_add_pin_date()));
                    item.setCom_id(applicationProperties.getCompanyId());
                    item.setStatus(status);
                    return item;
                })
                .writer(dataBaseWriter())
                .build();
    }


    @Bean("playlistInsertjob")
    public Job bookmarkInsertjob() {
        return jobBuilderFactory.get("playlistInsertjob")
                .incrementer(new RunIdIncrementer())
                .start(playlistCreatorStep())
                .build();
    }


}
