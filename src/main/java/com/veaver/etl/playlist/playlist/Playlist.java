package com.veaver.etl.playlist.playlist;

import lombok.Data;

import java.util.Date;

@Data
public class Playlist {
    Long idx;
    String folder_name;
    String folder_type;
    String user_key;
    int pin_count;
    Long thumbnail_timeline_idx;
    String recent_thumbnail;
    String delete_flag;
    Long upt_date;
    Date updDate;
    Long reg_date;
    Date regDate;
    String folder_type_name;
    Long last_add_pin_date;
    Date lastVideoAddDate;
    String com_id;
    String status;
}
