package com.veaver.etl.playlist.playlistvideo;

import lombok.Data;

import java.util.Date;

@Data
public class PlaylistVideo {
    Long idx;
    String user_key;
    Long folder_idx;
    Long timeline_idx;
    Integer begin;
    Integer end;
    String message;
    Long reg_date;
    Integer order_value;
    Date regDate;
    String com_id;
}
