package com.veaver.etl.playlist.playlistvideo;

import com.veaver.etl.configuration.BaseJobConfiguration;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;

@Configuration
public class PlaylistVideoCreatorJob extends BaseJobConfiguration {

    private static long count =0;

    private JdbcCursorItemReader<PlaylistVideo> databaseReader() {
        return databaseReader(asisDataSource, "SELECT IDX, USER_KEY, PN.FOLDER_IDX, " +
                "TIMELINE_IDX, BEGIN, END, MESSAGE, REG_DATE, ORDER_VALUE\n" +
                "FROM PIN PN INNER JOIN PIN_ORDER  PO ON PN.IDX = PO.PIN_IDX WHERE PN.ETL_STATUS=0", PlaylistVideo.class);
    }

    private JdbcBatchItemWriter<PlaylistVideo> insertTobeWriter() {

        return databaseWriter(tobeDataSource, "INSERT INTO T_PLAYLIST_VIDEO\n" +
                "(ORDER_VAL, REG_DATE, PLAY_LIST_ID, COMPANY_CONTENT_CONTRACT_ID, BEGIN, END, MESSAGE, OLD_ID, ETL_COM_ID)\n" +
                "VALUES(:order_value,:regDate,\n" +
                "(select ID from T_PLAYLIST P where P.OLD_ID=:folder_idx AND P.ETL_COM_ID=:com_id),\n" +
                "(select ID from T_COMPANY_CONTENT_CONTRACT CCC where CCC.OLD_TIMELINE_ID=:timeline_idx AND CCC.OWNER_COM_ID=:com_id ), " +
                ":begin, :end, :message, :idx, :com_id)");
    }

    private JdbcBatchItemWriter<PlaylistVideo> updateAsisWriter() {
        return databaseWriter(asisDataSource, "Update PIN SET ETL_STATUS=1 WHERE IDX=:idx");
    }

    private CompositeItemWriter<PlaylistVideo> dataBaseWriter() {
        CompositeItemWriter<PlaylistVideo> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }


    private Step playlistCreatorStep() {
        return stepBuilderFactory.get("playlistCreatorStep")
                .<PlaylistVideo, PlaylistVideo>chunk(500)
                .reader(databaseReader())
                .processor((ItemProcessor<PlaylistVideo, PlaylistVideo>) item -> {
                    item.setRegDate(item.getReg_date() == null ? null : longToDate(item.getReg_date()));
                    item.setCom_id(applicationProperties.getCompanyId());
                    return item;
                })
                .writer(dataBaseWriter())
                .build();
    }


    @Bean("playlistVideoInsertjob")
    public Job playlistVideoInsertjob() throws Exception {
        return jobBuilderFactory.get("playlistVideoInsertjob")
                .incrementer(new RunIdIncrementer())
                .start(playlistCreatorStep())
                .build();
    }


}
