package com.veaver.etl.userbookmark;

import lombok.Data;

@Data
public class AsisBookMark {
    Long idx;
    String title;
    String user_key;
    String new_flag;
    String user_list;
}
