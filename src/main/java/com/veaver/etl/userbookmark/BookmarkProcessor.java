package com.veaver.etl.userbookmark;

import com.veaver.etl.configuration.ApplicationProperties;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BookmarkProcessor implements ItemProcessor<AsisBookMark, TobeUserBookmark> {

    @Autowired
    ApplicationProperties applicationProperties;

    @Override
    public TobeUserBookmark process(AsisBookMark item) throws Exception {
        TobeUserBookmark tobeUserBookmark = new TobeUserBookmark();
        tobeUserBookmark.setBookmark_name(item.getTitle());
        tobeUserBookmark.setNew_flag(item.getNew_flag().equals("Y")?1:0);
        tobeUserBookmark.setUser_list(item.getUser_list());
        tobeUserBookmark.setUser_key(item.getUser_key());
        tobeUserBookmark.setCom_id(applicationProperties.getCompanyId());
        tobeUserBookmark.setOld_id(item.getIdx());
        return tobeUserBookmark;
    }
}
