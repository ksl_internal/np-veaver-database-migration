package com.veaver.etl.userbookmark;

import lombok.Data;

import java.util.Date;

@Data
public class TobeUserBookmark {
    String bkmk_id;
    Integer user_id;
    String bookmark_name;
    String user_list;
    Date reg_date;
    Date upt_date;
    Integer new_flag;
    String user_key;
    String com_id;
    Long old_id;
}
