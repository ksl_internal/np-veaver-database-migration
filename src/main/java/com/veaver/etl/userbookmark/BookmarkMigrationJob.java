package com.veaver.etl.userbookmark;

import com.veaver.etl.configuration.BaseJobConfiguration;
import com.veaver.etl.logo.LogoProcessor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;

@Configuration
public class BookmarkMigrationJob extends BaseJobConfiguration {

    @Autowired
    LogoProcessor logoProcessor;

    @Autowired
    BookmarkProcessor bookmarkProcessor;

    private JdbcCursorItemReader<AsisBookMark> databaseReader() {
        return databaseReader(asisDataSource, "SELECT IDX , TITLE , USER_KEY, NEW_FLAG, GROUP_CONCAT(UB.TARGET_USER_KEY SEPARATOR ', ') AS USER_LIST\n" +
                "FROM MST_USER_BOOKMARK MUB INNER JOIN USER_BOOKMARK UB ON MUB.IDX=UB.MST_BOOKMARK_IDX " +
                        "WHERE ETL_STATUS=0 " +
                        "GROUP BY IDX, TITLE, USER_KEY, NEW_FLAG ",
                AsisBookMark.class);
    }

    // ASIS DB does not have reg_date using CURRENT_TIMESTAMP
    private JdbcBatchItemWriter<TobeUserBookmark> insertTobeWriter() {
        return databaseWriter(tobeDataSource, "INSERT INTO T_USER_BOOKMARK\n" +
                "(BOOKMARK_NAME, USER_LIST, REG_DATE, UPD_DATE, USER_ID, NEW_FLAG, ETL_COM_ID)\n" +
                "    VALUES\n" +
                "(:bookmark_name, :user_list, CURRENT_TIMESTAMP, null, (SELECT ID FROM T_USER U WHERE U.USER_KEY=:user_key AND U.ETL_COM_ID=:com_id),:new_flag, :com_id)");
    }

    private JdbcBatchItemWriter<TobeUserBookmark> updateAsisWriter() {
        return databaseWriter(asisDataSource, "UPDATE MST_USER_BOOKMARK SET ETL_STATUS=1 WHERE IDX=:old_id");
    }

    private CompositeItemWriter<TobeUserBookmark> dataBaseWriter() {
        CompositeItemWriter<TobeUserBookmark> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }


    @Bean("bookmarkInsertjob")
    public Job bookmarkInsertjob() {
        return jobBuilderFactory.get("bookmarkInsertjob")
                .incrementer(new RunIdIncrementer())
                .start(bookmarkInsertStep())
                .build();
    }

    private Step bookmarkInsertStep() {
        return stepBuilderFactory.get("bookmarkInsertStep")
                .<AsisBookMark, TobeUserBookmark>chunk(500)
                .reader(databaseReader())
                .processor(bookmarkProcessor)
                .writer(dataBaseWriter())
                .build();
    }
}
