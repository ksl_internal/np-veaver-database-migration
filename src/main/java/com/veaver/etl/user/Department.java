package com.veaver.etl.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Department {
    Integer id; //DEPT_ID;
    String name; //DEPT_NAME;
    Integer level; //LEVEL;
    Integer parent_id; //PARENT_DEPT_ID;
    String com_id; //COM_ID;
}
