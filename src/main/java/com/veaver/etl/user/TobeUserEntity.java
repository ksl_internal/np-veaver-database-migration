package com.veaver.etl.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TobeUserEntity {
    String user_name;
    Integer dept_id;
    String email;
    String mobile_no;
    Date dob;
    String status;
    String full_name;
    String user_pwd;
    Integer position_id;
    String job;
    String group_name;
    Date invite_date;
    Date first_login_date;
    Date last_login_date;
    Integer login_fail_cnt;
    Date reg_date;
    Date upd_date;
    Integer user_id;
    Integer level;
    String previledge;
    String additional_data;
    String menu_permission;
    String duty;
    String id;
    String com_id;
    String com_role_cd;
    String user_key;
    String nick_name;
    String temp_password_flag;

    Date password_reset_req_date;

    String thumbnail;
    String thumbnail_small;
    String thumbnail_medium;
}
