package com.veaver.etl.user;

import com.veaver.etl.configuration.ApplicationProperties;
import com.veaver.etl.department.MyTreeNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Slf4j
@Component
public class UserInfoItemProcessor  implements ItemProcessor<AsisUserInfo, TobeUserEntity> {

    Random random = new Random();

    @Autowired
    ApplicationProperties applicationProperties;

    public static long count = 0;

    @Override
    public TobeUserEntity process(AsisUserInfo item) throws Exception {
        TobeUserEntity tobeUserEntity = new TobeUserEntity();

        tobeUserEntity.setDob(item.getDob()==null?null:new Date(Integer.parseInt(item.getDob())));
        tobeUserEntity.setDuty(item.getDuty());

        tobeUserEntity.setGroup_name(item.getGroup_name());
        tobeUserEntity.setJob(item.getJob());
        tobeUserEntity.setLevel(item.getLevel()==null?null:Integer.parseInt(item.getLevel()));
        tobeUserEntity.setLogin_fail_cnt(item.getLogin_fail_count()==null?null:Integer.parseInt(item.getLogin_fail_count()));
        tobeUserEntity.setMenu_permission(item.getMenu_permission());
        tobeUserEntity.setMobile_no(item.getMobile_no());

        tobeUserEntity.setUser_name(item.getEmail()==null?"email"+System.currentTimeMillis()+random.nextInt(1000)+"@veaver.com":item.getEmail());
        tobeUserEntity.setEmail(item.getEmail()==null?"email"+System.currentTimeMillis()+random.nextInt(1000)+"@veaver.com":item.getEmail());

        String status="REGISTERED";
        if(item.getDelete_flag().equals("Y"))
            status="DELETED";
        else if(item.getActivation_flag().equals("N"))
            status="SUSPENDED";
        else if(item.getAuth_status().equals("A"))
            status="ACTIVE";

        tobeUserEntity.setStatus(status);
        tobeUserEntity.setNick_name(item.getNickname());
        tobeUserEntity.setUser_pwd(item.getUser_pwd());

        Integer dept_id = findDepartmentId(DepartmentTreeIndexCreatorTasklet.root, getDepthAsList(item), 0);
        tobeUserEntity.setDept_id(dept_id);
        tobeUserEntity.setCom_id(applicationProperties.getCompanyId());
        tobeUserEntity.setCom_role_cd(applicationProperties.getCompanyRoleCode());
        tobeUserEntity.setInvite_date(item.getInvite_date()==null?null:new Date(Long.parseLong(item.getInvite_date())));
        tobeUserEntity.setFirst_login_date(item.getFirst_login_date()==null?null:new Date(Long.parseLong(item.getFirst_login_date())));
        tobeUserEntity.setLast_login_date(item.getLast_login_date()==null?null:new Date(Long.parseLong(item.getLast_login_date())));
        tobeUserEntity.setReg_date(item.getReg_date()==null?null:new Date(Long.parseLong(item.getReg_date())));
        tobeUserEntity.setUpd_date(item.getUpt_date()==null?null:new Date(Long.parseLong(item.getUpt_date())));
        tobeUserEntity.setUser_key(item.getUser_key());
        tobeUserEntity.setTemp_password_flag(item.getTemp_password_flag());
        tobeUserEntity.setPassword_reset_req_date((item.getPassword_reset_req_date()==null)?null:new Date(item.getPassword_reset_req_date()));

        tobeUserEntity.setThumbnail(item.getThumbnail());
        tobeUserEntity.setThumbnail_small(item.getThumbnail_small());
        tobeUserEntity.setThumbnail_medium(item.getThumbnail_medium());

        log.debug("Count {}, UserKey {}",count++,item.getUser_key());
        return tobeUserEntity;
    }

    private Integer findDepartmentId(MyTreeNode<Department> start, List<String> dataList, int index){
        if(index >= dataList.size() || start.getChildren()==null || start.getChildren().isEmpty())
            return start.getData().getId();

        for(MyTreeNode<Department> department : start.getChildren()){
            String departmentName = dataList.get(index);
            if(department.getData().getName().equals(departmentName)){
                return findDepartmentId(department, dataList , index+1);
            }
        }
        return null;
    }

    private List<String> getDepthAsList(AsisUserInfo item){
        List<String> result = new ArrayList<>();
        if(item.getDepth_1() == null)
            return result;
        result.add(item.getDepth_1());

        if(item.getDepth_2() == null)
            return result;
        result.add(item.getDepth_2());

        if(item.getDepth_3() == null)
            return result;
        result.add(item.getDepth_3());

        if(item.getDepth_4() == null)
            return result;
        result.add(item.getDepth_4());

        if(item.getDepth_5() == null)
            return result;
        result.add(item.getDepth_5());

        return result;
    }
}
