package com.veaver.etl.user;

import com.veaver.etl.configuration.ApplicationProperties;
import com.veaver.etl.department.MyTreeNode;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DepartmentTreeIndexCreatorTasklet implements Tasklet {

    private JdbcTemplate jdbcTemplate;
    private ApplicationProperties applicationProperties;

    private Map<Integer, Department> departmentMap = new HashMap<>();

    public static MyTreeNode<Department> root = new MyTreeNode<>(new Department());

    public DepartmentTreeIndexCreatorTasklet(JdbcTemplate jdbcTemplate, ApplicationProperties applicationProperties) {
        this.jdbcTemplate = jdbcTemplate;
        this.applicationProperties = applicationProperties;
    }

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        List<Department> departmentList = jdbcTemplate.query(
                String.format("SELECT * FROM T_DIVISIONAL_HIERARCHY WHERE COM_ID='%s' AND TYPE='DEPARTMENT'",
                        applicationProperties.getCompanyId()),
                new BeanPropertyRowMapper(Department.class));
        for (Department department : departmentList) {
            if(department.getParent_id()==null){
                root.addChild(department);
                continue;
            }
            departmentMap.put(department.getId(), department);

        }
        createTree(root);
        return RepeatStatus.FINISHED;
    }

    private void createTree(MyTreeNode<Department> treeNode) {
        if(treeNode.getChildren() == null || treeNode.getChildren().isEmpty())
            return;
        for(MyTreeNode<Department> child : treeNode.getChildren()){
            int idx = child.getData().getId();
            List<Integer> keyList = new ArrayList<>(departmentMap.keySet());
            for(Integer key : keyList){
                Department department = departmentMap.get(key);
                if(department == null || department.getParent_id()==null)
                    continue;
                if(department.getParent_id()==idx){
                    child.addChild(department);
                    departmentMap.remove(key);
                }
            }
            createTree(child);
        }
    }


}
