package com.veaver.etl.user;

import com.veaver.etl.configuration.BaseJobConfiguration;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;

@Configuration
public class UserCreatorJob extends BaseJobConfiguration {

    @Autowired
    UserInfoItemProcessor userInfoItemProcessor;

    @Autowired
    UserInfoItemWriter userInfoItemWriter;

    private JdbcBatchItemWriter<TobeUserEntity> insertTobeWriter() {
        return databaseWriter(tobeDataSource, "INSERT into T_USER\n" +
                "(USER_NAME, COM_ID, NICK_NAME, DEPT_ID, EMAIL, MOBILE_NO, DOB, STATUS, FULL_NALE, USER_PWD, " +
                "THUMBNAIL, THUMBNAIL_SMALL, THUMBNAIL_MEDIUM, POSITION_ID, JOB, GROUP_NAME, INVITE_DATE, \n" +
                " FIRST_LOGIN_DATE, LAST_LOGIN_DATE, LOGIN_FAIL_CNT, REG_DATE, UPD_DATE, LEVEL, PREVILEDGE, ADDITIONAL_DATA, \n" +
                " MENU_PERMISSION, DUTY, TEMP_PASSWORD_FLAG, PASSWORD_RESET_REQ_DATE, ETL_COM_ID, USER_KEY)\n" +
                "VALUES\n" +
                "(:user_name,:com_id,:nick_name, :dept_id, :email, :mobile_no, :dob, :status, :full_name, :user_pwd, " +
                ":thumbnail, :thumbnail_small, :thumbnail_medium, :position_id, :job, :group_name,:invite_date, \n" +
                ":first_login_date, :last_login_date, :login_fail_cnt, :reg_date, :upd_date,:level, :previledge, :additional_data, \n" +
                ":menu_permission, :duty, :temp_password_flag, :password_reset_req_date, :com_id, :user_key)");
    }

    private JdbcBatchItemWriter<TobeUserEntity> updateAsisWriter() {
        return databaseWriter(asisDataSource, "UPDATE `USER` U SET ETL_STATUS=1 WHERE U.USER_KEY=:user_key");
    }

    private CompositeItemWriter<TobeUserEntity> dataBaseWriter() {
        CompositeItemWriter<TobeUserEntity> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }

    private JdbcCursorItemReader<AsisUserInfo> databaseReader() {
        return databaseReader(asisDataSource, "select \n" +
                "\tU.EMAIL AS USER_NAME, \n" +
                "\tU.EMAIL AS EMAIL,\n" +
                "\tUPR.PHONE AS MOBILE_NO,\n" +
                "\tUPR.BIRTH AS DOB,\n" +
                "\tU.PASSWORD AS USER_PWD,\n" +
                "\tUPR.JOB AS JOB,\n" +
                "\tUPR.GROUP AS GROUP_NAME,\n" +
                "\tU.DELETE_FLAG AS DELETE_FLAG,\n" +
                "\tU.ACTIVATION_FLAG AS ACTIVATION_FLAG,\n" +
                "\tU.AUTH_STATUS AS AUTH_STATUS,\n" +
                "\tU.NICKNAME AS NICKNAME,\n" +
                "\tU.INVITE_DATE AS INVITE_DATE,\n" +
                "\tU.FIRST_LOGIN_DATE AS FIRST_LOGIN_DATE,\n" +
                "\tU.LAST_LOGIN_DATE AS LAST_LOGIN_DATE,\n" +
                "\tU.TEMP_PASSWORD_FLAG AS TEMP_PASSWORD_FLAG,\n" +
                "\tU.PASSWORD_RESET_REQ_DATE AS PASSWORD_RESET_REQ_DATE,\n" +
                "\tUP.LOGIN_FAIL_COUNT AS LOGIN_FAIL_COUNT,\n" +
                "\tU.REG_DATE AS REG_DATE,\n" +
                "\tU.UPT_DATE AS UPD_DATE,\n" +
                "\tUP.LEVEL AS LEVEL,\n" +
                "\tUP.ADMIN_PERM_MENU_ROLE AS MENU_PERMISSION,\n" +
                "\tUPR.DUTY AS DUTY,\n" +
                "\tUPR.`1_DEPTH` AS DEPTH_1,\n" +
                "\tUPR.`2_DEPTH` AS DEPTH_2,\n" +
                "\tUPR.`3_DEPTH` AS DEPTH_3,\n" +
                "\tUPR.`4_DEPTH` AS DEPTH_4,\n" +
                "\tUPR.`5_DEPTH` AS DEPTH_5,\n" +
                "\tU.USER_KEY AS USER_KEY\n" +
                "from `USER` U LEFT JOIN USER_PERMISSION UP on U.USER_KEY = UP.USER_KEY LEFT JOIN USER_PROFILE UPR on U.USER_KEY=UPR.USER_KEY " +
                        "WHERE U.ETL_STATUS=0",
                AsisUserInfo.class);
    }

    @Bean("treeIndexCreateJob")
    public Job treeIndexCreatorJob() {
        return jobBuilderFactory.get("treeIndexCreatorJob")
                .incrementer(new RunIdIncrementer())
                .start(treeIndexCreatorStep())
                .build();
    }

    private Step treeIndexCreatorStep() {
        return stepBuilderFactory.get("treeIndexCreatorStep")
                .tasklet(new DepartmentTreeIndexCreatorTasklet(jdbcTemplate,applicationProperties))
                .build();
    }

    @Bean("userCreateJob")
    public Job userCreatorJob() {
        return jobBuilderFactory.get("userCreatorJob")
                .incrementer(new RunIdIncrementer())
                .start(userCreatorStep())
                .build();
    }

    private Step userCreatorStep() {
        return stepBuilderFactory.get("userCreatorStep")
                .<AsisUserInfo, TobeUserEntity>chunk(500)
                .reader(databaseReader())
                .processor(userInfoItemProcessor)
                .writer(dataBaseWriter())
                .build();
    }

}
