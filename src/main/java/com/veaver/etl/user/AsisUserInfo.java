package com.veaver.etl.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AsisUserInfo {
    String user_name;
    String email;
    String mobile_no;
    String dob;
    String user_pwd;
    String job;
    String group_name;
    String thumbnail;
    String thumbnail_small;
    String thumbnail_medium;
    String delete_flag;
    String activation_flag;
    String auth_status;
    String nickname;
    String invite_date;
    String first_login_date;
    String last_login_date;
    String login_fail_count;
    String reg_date;
    String upt_date;
    String level;
    String menu_permission;
    String duty;
    String depth_1;
    String depth_2;
    String depth_3;
    String depth_4;
    String depth_5;
    String user_key;
    String temp_password_flag;
    Long password_reset_req_date;
}
