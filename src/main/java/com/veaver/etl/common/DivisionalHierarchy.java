package com.veaver.etl.common;

import lombok.Data;

import java.util.Date;

@Data
public class DivisionalHierarchy {
    Integer old_id;
    String name;
    String additionalData;
    String description;
    Integer level;
    String type;
    Integer parent_id;
    Integer old_parent_id;
    Date reg_date;
    Date upd_date;
    String com_id;
}
