package com.veaver.etl.device;

import com.veaver.etl.configuration.BaseJobConfiguration;
import com.veaver.etl.logo.LogoProcessor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;

@Configuration
public class DeviceMigrationJob extends BaseJobConfiguration {

    @Autowired
    LogoProcessor logoProcessor;



    @Autowired
    DeviceProcessor deviceProcessor;

    private JdbcCursorItemReader<AsisDevice> databaseReader() {
        return databaseReader(asisDataSource, "SELECT UD.USER_KEY, DE.IDX, DE.DEVICE_ID, DE.DEVICE_TOKEN, " +
                        "DE.DEVICE_OS, DE.DEVICE_MODEL, DE.COUNTRY_CODE, " +
                        "DE.LANGUAGE_CODE, DE.ENDPOINT_ARN, DE.ALL_SUBSCRIBE_ARN, DE.OSTYPE_SUBSCRIBE_ARN, DE.REG_DATE,  " +
                        "DE.APP_VERSION " +
                        "FROM DEVICE DE " +
                        "INNER JOIN " +
                        "USER_DEVICE AS UD ON DE.IDX = UD.DEVICE_IDX WHERE DE.ETL_STATUS=0 ",
                AsisDevice.class);
    }


    private JdbcBatchItemWriter<TobeDevice> insertTobeWriter() {
        return databaseWriter(tobeDataSource, "INSERT INTO T_DEVICE\n" +
                "(DEVICE_UID, USER_ID, DEVICE_TOKEN, OS_NAME,DEVICE_MODEL, COUNTRY, LANGUAGE, ENDPOINT_ARN," +
                "ALL_SUBSCRIBE_ARN, OSTYPE_SUBSCRIBE_ARN, STATUS, REG_DATE, OLD_ID, ETL_COM_ID)" +
                "VALUES(:device_id, (SELECT ID FROM T_USER U WHERE U.USER_KEY=:user_key AND U.ETL_COM_ID=:com_id), :device_token, " +
                ":device_os, :device_model, :country_code," +
                ":language_code, :endpoint_arn, :all_subscribe_arn, :ostype_subscribe_arn,'ACTIVE', :reg_date, :old_id, :com_id)");
    }

    private JdbcBatchItemWriter<TobeDevice> updateAsisWriter() {
        return databaseWriter(asisDataSource, "UPDATE DEVICE D SET ETL_STATUS=1 WHERE D.IDX=:old_id");
    }

    private CompositeItemWriter<TobeDevice> dataBaseWriter() {
        CompositeItemWriter<TobeDevice> compositeItemWriter = new CompositeItemWriter<>();
        if(applicationProperties.getStatusUpdateEnabled()) {
            compositeItemWriter.setDelegates(Arrays.asList(insertTobeWriter(), updateAsisWriter()));
        }else{
            compositeItemWriter.setDelegates(Collections.singletonList(insertTobeWriter()));
        }
        return compositeItemWriter;
    }

    @Bean("deviceInsertjob")
    public Job deviceInsertjob() {
        return jobBuilderFactory.get("deviceInsertjob")
                .incrementer(new RunIdIncrementer())
                .start(deviceInsertStep())
                .build();
    }

    private Step deviceInsertStep() {
        return stepBuilderFactory.get("deviceInsertStep")
                .<AsisDevice, TobeDevice>chunk(500)
                .reader(databaseReader())
                .processor(deviceProcessor)
                .writer(dataBaseWriter())
                .build();
    }
}
