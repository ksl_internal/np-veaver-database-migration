package com.veaver.etl.device;

import com.veaver.etl.configuration.ApplicationProperties;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class DeviceProcessor implements ItemProcessor<AsisDevice, TobeDevice> {
    @Autowired
    protected ApplicationProperties applicationProperties;

    @Override
    public TobeDevice process(AsisDevice item) throws Exception {
        TobeDevice tobeDevice = new TobeDevice();
        tobeDevice.setDevice_id(item.getDevice_id());
        tobeDevice.setDevice_token(item.getDevice_Token());
        tobeDevice.setDevice_os(item.getDevice_os());
        tobeDevice.setDevice_model(item.getDevice_model());
        tobeDevice.setCountry_code(item.getCountry_code());
        tobeDevice.setLanguage_code(item.getLanguage_code());
        tobeDevice.setEndpoint_arn(item.getEndpoint_arn());
        tobeDevice.setAll_subscribe_arn(item.getAll_subscribe_arn());
        tobeDevice.setOstype_subscribe_arn(item.getOstype_subscribe_arn());
        tobeDevice.setApp_version(item.getApp_version());
        tobeDevice.setReg_date(item.getReg_date()==null?null:new Date(item.getReg_date()));
        tobeDevice.setUser_key(item.getUser_key());
        tobeDevice.setOld_id(item.getIdx());
        tobeDevice.setCom_id(applicationProperties.getCompanyId());


        return tobeDevice;
    }
}
