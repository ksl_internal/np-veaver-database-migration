package com.veaver.etl.device;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AsisDevice {
    Long idx;

    String device_id;

    String device_Token;

    String device_os;

    String device_model;

    String country_code;

    String language_code;

    String endpoint_arn;

    String all_subscribe_arn;

    String ostype_subscribe_arn;

    Long reg_date;

    String app_version;

    String user_key;

}