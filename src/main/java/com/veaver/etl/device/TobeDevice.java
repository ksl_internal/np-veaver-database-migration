package com.veaver.etl.device;

import lombok.Data;

import java.util.Date;

@Data
public class TobeDevice {

    String device_id;
    String device_token;
    String device_os;
    String device_model;
    String country_code;
    String language_code;
    String endpoint_arn;
    String all_subscribe_arn;
    String ostype_subscribe_arn;
    Date reg_date;
    String app_version;
    String user_key;
    Long old_id;
    String com_id;
}
