# Database Migration Tool for Veaver Service V2

## Instructions

### DB prerequisites
1. Add ETL_STATUS column in V1 tables. Apply prepare-asis-db-for-etl.sql in V1 DB.
2. Apply V2 Schema in V1 DB. Apply ddl-all.sql in V1 DB.
2. Insert Compnay Info as SQL insert. Edit compnay-ddl.sql and apply in V1 DB.